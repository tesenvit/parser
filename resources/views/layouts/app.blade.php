<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <!-- SEO -->
    @include('layouts.seo_module_head')

    <meta name="google-site-verification" content="1LhCK0DZ9zQhB61ntPXH5ntRMQ7n0C_4Du9qN3Ja3wE" />

    <!-- Styles -->
    <link href="{{ asset('css/app.css?v=1.5') }}" rel="stylesheet">
    <link href="{{ asset('css/grid.css?v=1.4') }}" rel="stylesheet">
    <link href="{{ asset('css/myApp.css?v=5.6') }}" rel="stylesheet">
    <link href="{{ asset('js/EasyAutocomplete/easy-autocomplete.min.css') }}" rel="stylesheet">
    <link href="{{ asset('vendor/magnific-popup/magnific-popup.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.1.0/css/all.css"
          integrity="sha384-lKuwvrZot6UHsBSfcMvOkWwlCMgc0TaWr+30HWe3a4ltaBwTZhyTEggF5tJv8tbt" crossorigin="anonymous">
    <link href="{{ asset('vendor/forms/form.css') }}" rel="stylesheet">
    <script src="{{ asset('vendor/forms/form.js') }}" defer></script>

    {{--<script src="{{ asset('js/app.js') }}"></script>--}}

    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.inview/1.0.0/jquery.inview.min.js"></script>

    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"
            integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa"
            crossorigin="anonymous"></script>


    <script src="{{ asset('js/EasyAutocomplete/jquery.easy-autocomplete.min.js') }}"></script>
    <script src="{{ asset('js/myApp.js?v=2.4') }}"></script>
    <script src="{{ asset('vendor/magnific-popup/jquery.magnific-popup.min.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/fingerprintjs2/2.0.6/fingerprint2.min.js"></script>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css"
          integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ" crossorigin="anonymous">
    <script>
        window.authCheck = "{{ Auth::check() }}"
    </script>
</head>
<body>
<div id="app">
    <nav class="navbar navbar-default navbar-static-top {{ Auth::check() ? "auth-check-navbar" : "" }}">
        <div class="container container-navbar">
            <div class="navbar-header a">
                <!-- Branding Image -->
                <a class="logo-link" href="{{ url('/') }}">
                    {{--{{ config('app.name', 'unni.io') }}--}}
                    <img src="/image/unni_logo.png" alt="" class="logo">
                </a>
            </div>

            {{--<div class="header__facebook-link">--}}
            <div class="header__facebook-link">
                <a href="/pages/lenta-sobytiy-facebook">Лента facebook</a>
            </div>

            @guest()
                <div class="navbar-right main-navbar-login">
                    <button type="button" class="dropbtn nav-login" data-toggle="modal" data-target="#login-with-social">
                        Войти
                    </button>
                </div>
            @endguest

            @auth
                <div class="dropdown navbar-right navbar-profile">
                    <button class="dropbtn navbar-profile-dropdown">
                        <img src="{{ Auth::user()->avatar }}" class="img-circle" width="25" height="25" alt="avatar">
                        <span class="navabr-profile-block-text text-center">
                            <span class="navabr-profile-text">Профиль</span>
                            <span class="caret"></span>
                        </span>
                    </button>
                    <div class="dropdown-content">
                        <a href="{{ route('home') }}">Кабинет</a>
                        <a href="{{ route('user.logout') }}">Выход</a>
                    </div>
                </div>
            @endauth

            <div class="dropdown navbar-right navbar-hamburger">
                <button class="dropbtn"><i class="fas fa-bars fa-lg"></i></button>
                <div class="dropdown-content">
                    <a href="{{ route('front.feeds') }}">Лента</a>
                    <a href="{{ route('front.organizers') }}">IT сайты</a>
                    <a href="/pages/partners">Партнеры</a>
                    <a href="/pages/about-us">О нас</a>
                    <a href="{{ route('front.profile.site.add') }}">Добавить сайт</a>
                </div>
            </div>
        </div>
    </nav>

    @yield('content')
</div>

<!-- SEO -->
@include('layouts.seo_module_footer')

<footer>
    <a href="/pages/privacy">Политики конфиденциальности</a>
    <a href="https://www.facebook.com/Giid.search" target="_blank" style="padding-left: 30%">
        <i class="fab fa-facebook-f" style="font-size: 25px;"></i>
    </a>
</footer>

@include('auth.widget_login')

<!-- Scripts -->
{{--<script src="{{ asset('js/app.js') }}"></script>--}}
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-128551053-1"></script>
<script>
    window.dataLayer = window.dataLayer || [];

    function gtag() {
        dataLayer.push(arguments);
    }

    gtag('js', new Date());

    gtag('config', 'UA-128551053-1');
</script>
<script id="userFixLink">
    if (window.requestIdleCallback) {
        requestIdleCallback(function () {
            Fingerprint2.get(function (components) {
                let imprint = Fingerprint2.x64hash128(components.map(function (pair) {
                    return pair.value
                }).join(), 31);
                $('#load li div a').on('click', function () {
                    let link = $(this).attr('href');
                    let source_page = document.location.href;
                    $.ajax({
                        type: "POST",
                        url: "/follow/click",
                        data: {"link": link, "source_page": source_page, "imprint": imprint},
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        // cache: false,
                        success: function (data) {
                        }
                    });
                    $('#userFixLink').remove();
                });
            })
        })
    } else {
        setTimeout(function () {
            Fingerprint2.get(function (components) {
                var imprint = Fingerprint2.x64hash128(components.map(function (pair) {
                    return pair.value
                }).join(), 31);
                // console.log(components) // an array of components: {key: ..., value: ...}
            })
        }, 500)
    }
</script>

<script>
    let navHeight = $('.navbar.navbar-default.navbar-static-top').height();
    $('.navbar-hamburger button').css('height', navHeight)
</script>

@yield('script')
</body>
</html>
