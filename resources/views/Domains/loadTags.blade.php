<div id="load">
    @foreach($tags as $domainTag)
        <a href="/?search={{$domainTag->tag}}&typeSearch=search_name">
            <span class="tags-link">{{$domainTag->tag}} ({{$domainTag->count}})</span>
        </a>
    @endforeach
    {{ $tags->links('paginate_one') }}
</div>