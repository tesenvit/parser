<div class="col-sm-7 col-md-7 col-xs-12 col-lg-4 panel panel-default modal-block-center" id="load">
    <div class="panel-heading text-center"><h3>Предложить тег для домена!</h3></div>
    <div class="panel-body">
        <form class="customForm" method="POST" action="{{ route('tags.add-user') }}">
            {{ csrf_field() }}
            <div class="input {{ $errors->has('password') ? ' has-error' : '' }}">
                    <label for="password">Укажите тег</label>
                    <input id="tag" type="text" name="tag" required autocomplete="off">
                    @if ($errors->has('password'))
                        <span class="help-block"><strong>{{ $errors->first('password') }}</strong></span>
                    @endif
            </div>
            <input type="hidden" name="id" value="" class="addTagId">
            <div class="sendmail">
                <button type="submit">Отправить</button>
            </div>
        </form>
        <div class="alert alert-danger" style="display:none;"></div>
        <div class="alert alert-success" style="display:none;"></div>
    </div>
</div>
<link href="{{ asset('vendor/forms/form.css') }}" rel="stylesheet">
<script src="{{ asset('vendor/forms/form.js') }}"></script>
<script>
    $('.sendmail').on('click', function () {
        $('#load').css('background', '#fff');
        $('#load').append('<img style="position: fixed; left: 40%; top: 40%; z-index: 100000;" src="/images/loading.gif" />');
    });
</script>
