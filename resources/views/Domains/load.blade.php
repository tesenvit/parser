<div id="load" >
    @foreach($domainContents as $domainContent)
    @php $parseURL = parse_url($domainContent->linksContent->url) @endphp
        <li class="domain-tags">
            <div class="col-8">
                <div>
                    <p class="feed-title">{{ $domainContent->title }}</p>
                </div>

                <div>
                    <noindex>
                        <a href="{{ $parseURL ? $domainContent->linksContent->url : 'http://'.$name_domains }}" target="_blank" class="feed-url-on-original" rel="nofollow">
                            {{ $parseURL['path'] ?? 'http://'.$name_domains }}
                        </a>
                    </noindex>
                </div>

                <div>
                    @if($domainContent->description != 1)
                        {{ $domainContent->description }}
                    @endif
                </div>

                <div>{{ $domainContent->created_page }}</div>

            </div>
            <div class="col-4 domains-image">
                <a class="images_popup" href="{{ $domainContent->image }}">
                    <img src="{{ $domainContent->image }}" alt="">
                </a>
            </div>

            <div class="col-12" id="feed-tags">
                @foreach ($domainContent->tags as $tag)
                    <span>
                        <a href="/?search={{$tag->tag}}&typeSearch=search_name" class="tags-link domains-tag-link">{{$tag->tag}}</a>
                    </span>
                @endforeach
            </div>
        </li>
    @endforeach
    {{ $domainContents->links('paginate_one') }}
</div>