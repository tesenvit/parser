@extends('Admin/layouts.app')
@section('content')
    <div class="row">
        <div class="col-12">
            <h1 class="form-group">Список организаторов</h1>
            <div class="panel panel-default">
                <div class="panel-body">
                    <a href="{{ route('admin.organizers.create') }}" class="btn btn-success">Добавить организатора</a>
                    <br>
                    <br>

                    @if( session()->has('success') && !empty(session('success')))
                        <div class="alert alert-success alert-dismissible fade show" role="alert">
                            <strong>{{ session('success') }}</strong>
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    @endif

                    <table class="table table-bordered table-striped">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Наименование</th>
                            <th>FB id</th>
                            <th>Адрес сайта</th>
                            <th>Дата создания</th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($organizers as $organizer)
                            <tr>
                                <td>{{$organizer->id}}</td>
                                <td>{{$organizer->name}}</td>
                                <td>{{$organizer->fb_group_id}}</td>
                                <td><a href="{{$organizer->website}}">Перейти</a></td>
                                <td>{{$organizer->created_at}}</td>
                                <td>
                                    <a href="{{ route('admin.organizers.show', $organizer->id) }}" class="btn btn-success">Редактировать</a>
                                    <form action="{{ route('admin.organizers.destroy', $organizer->id) }}" method="post">
                                        <input type="hidden" name="_method" value="DELETE">
                                        {{ csrf_field() }}
                                        <button class="btn btn-danger">Удалить</button>
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    {{ $organizers->links('paginate') }}
                </div>
            </div>
        </div>
    </div>

@endsection