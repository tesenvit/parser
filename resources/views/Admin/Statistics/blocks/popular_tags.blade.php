<table class="table">
    <thead>
    <tr>
        <th>Название</th>
        <th>первый раз найден</th>
        <th>Последний раз обновлен</th>
        <th>Кол-во</th>
    </tr>
    </thead>
    <tbody>
    @foreach($popularTags as $tag)
        <tr>
            <td data-label="Название">{{ $tag->tag }}</td>
            <td data-label="первый раз найден">{{ $tag->created_at }}</td>
            <td data-label="Последний раз обновлен">{{ $tag->updated_at }}</td>
            <td data-label="Кол-во">{{ $tag->count }}</td>
        </tr>
    @endforeach
    {{ $popularTags->links('paginate_one') }}
    </tbody>
</table>