<div id="myModals" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <form action="" id="editDomain">
                    {{ csrf_field() }}
                    <div class="form-group editDomain">
                        <label for="editDomain">
                            Укажите адрес домена без http или https.
                        </label>

                    </div>

                    <div class="modal-footer">
                        <button class="change btn btn-success">
                            Отредактировать
                        </button>
                        <button class="btn btn-default closeDomain" type="button" data-dismiss="modal">
                            Закрыть
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>