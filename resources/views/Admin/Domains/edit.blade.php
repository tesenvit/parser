{{$adminURL = env('APP_ADMIN_URL', 'test')}}
@extends('Admin/layouts.app')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-lg-12 domainPage">
                <h1 class="col-lg-12">Редактирование домена {{ $domain->domain }}</h1>
                <br>
                <div class="panel panel-default">
                    <div class="panel-body">
                        <form method="post" action="{{ route('admin.domains.update', $domain->id) }}" enctype="multipart/form-data">

                            <input type="hidden" name="_token" value="{{csrf_token()}}">
                            <input type="hidden" name="domain_id" value="{{$domain->id}}">

                            <input type="hidden" name="_method" value="PUT">

                            <div class="row">

                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <label for="">Имя домена</label>
                                        <input class="form-control" type="text" name="name"
                                               value="{{ $domain->domain or '' }}">
                                    </div>
                                </div>

                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <label for="">Теги домена</label>
                                        <select class="tags-input" multiple name="tags[]">
                                            @foreach ($tags as $item)
                                                <option value="{{ $item->id }}" selected>{{ $item->tag }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <label for="">SEO title</label>
                                        <input class="form-control" type="text" name="seo_title" value="{{ $domain->seo_title or '' }}">
                                    </div>
                                </div>

                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <label for="">SEO description</label>

                                        <textarea class="form-control" name="seo_description" id="" cols="10" rows="4">{{ $domain->seo_description or '' }}</textarea>

                                    </div>
                                </div>

                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <label for="">Описание</label>
                                        <textarea class="form-control" name="description" id="" cols="10" rows="4">{{ $domain->description or '' }}</textarea>
                                    </div>
                                </div>

                                <div class="col-lg-12">
                                    <p class="mb-1">Лого (макс. 400x400)</p>
                                    <div class="form-group">
                                        <img src="/storage/{{ $domain->logo }}" alt="img" width="150" height="150" class="pb-3">
                                        <input type="file" class="form-control-file" id="" name="logo">
                                    </div>
                                </div>

                                <div class="col-lg-2 pt-2">
                                    <div class="form-group">
                                        <label for="">Приоритет</label>
                                        <input class="form-control" type="text" name="priority"
                                               value="{{ $domain->priority or '' }}">
                                    </div>
                                </div>

                                <div class="col-lg-12 pt-2">
                                    <input type="checkbox" class="checkbox" name="enableDomain" id="{{$domain->id}}" {{ $domain->parser_content == '1' ? 'checked' : '' }}/>
                                    <label for="{{$domain->id}}">Парсить сайт</label>
                                </div>

                                <div class="col-lg-12 pt-2">
                                    <input type="checkbox" class="checkbox" name="status_delete" value="1" id="delete_{{$domain->id}}" {{ $domain->status_delete == '1' ? 'checked' : '' }}/>
                                    <label for="delete_{{$domain->id}}">Удалять контент</label>
                                </div>

                                <div class="col-lg-12 pt-2">
                                    <input type="submit" value="Сохранить" class="btn btn-success">
                                </div>

                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection