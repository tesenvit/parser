@extends('Admin/layouts.app')
@section('content')
    @if (session('status'))
        <div class="alert alert-success">
            {{ session('status') }}
        </div>
    @endif
    <div class="row">
        <div class="col-12">
            <h1>Список приоритетных тегов</h1>

            <div class="form-row mt-4 mb-3">
                <div class="form-group col-md-1">
                    <a href="{{ route('admin.tags.add_tags') }}" class="btn btn-success">Добавить тег</a>
                </div>
                <div class="form-group col-md-2">
                    <input type="text" class="form-control" id="tag-search" name="search" placeholder="Поиск">
                </div>
            </div>

            <div id="tags-content">
                @include('Admin.Tags.content')
            </div>

            {{ $tags->links('paginate') }}

        </div>
    </div>
@endsection