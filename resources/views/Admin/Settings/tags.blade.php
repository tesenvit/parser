@extends('Admin/layouts.app')
@section('content')
    @if (session('status'))
        <div class="alert alert-success">
            {{ session('status') }}
        </div>
    @endif
    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="panel panel-default">
                    <div class="panel-body delete-parser">
                        <h3>Параметры для тегов</h3>
                        <form action="{{ route('admin.settings.tagsPost') }}" method="post">
                            {{ csrf_field() }}
                            <table class="table information_json">
                                <tr>
                                    <th>Название поля</th>
                                    <th>Значение поля</th>
                                    <th></th>
                                </tr>
                                @foreach ($params as $param)
                                    <tr>
                                        <td>
                                            <input type="text" class="form-control" id="information_json_name[]" placeholder="Название поля" name="name[]" value="{{ $param->name }}">
                                        </td>
                                        <td>
                                            <input type="text" class="form-control" id="information_json_val[]" placeholder="Значение поля" name="value[]" value="{{ $param->value }}">
                                        </td>
                                        <td>
                                            <span class="btn btn-danger minus pull-right">–</span>
                                        </td>
                                    </tr>
                                @endforeach
                                <tr class="information_json_plus">
                                    <td></td>
                                    <td></td>
                                    <td><span class="btn btn-success plus pull-right">+</span></td>
                                </tr>
                            </table>
                            <button class="btn btn-xs btn-success">
                                Добавить
                            </button>
                        </form>
                        <h3>Параметры для изображений</h3>
                        <form action="{{ route('admin.settings.tagsPost') }}" method="post">
                            {{ csrf_field() }}
                            <table class="table information_json">
                                <tr>
                                    <th>Название поля</th>
                                    <th>Значение поля</th>
                                    <th></th>
                                </tr>
                                @foreach ($params as $param)
                                    <tr>
                                        <td>
                                            <input type="text" class="form-control" id="information_json_name[]" placeholder="Название поля" name="name[]" value="{{ $param->name }}">
                                        </td>
                                        <td>
                                            <input type="text" class="form-control" id="information_json_val[]" placeholder="Значение поля" name="value[]" value="{{ $param->value }}">
                                        </td>
                                        <td>
                                            <span class="btn btn-danger minus pull-right">–</span>
                                        </td>
                                    </tr>
                                @endforeach
                                <tr class="information_json_plus">
                                    <td></td>
                                    <td></td>
                                    <td><span class="btn btn-success plus pull-right">+</span></td>
                                </tr>
                            </table>
                            <input type="text" name="type" value="image">
                            <button class="btn btn-xs btn-success">
                                Добавить
                            </button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection