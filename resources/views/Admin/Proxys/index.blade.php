@extends('Admin/layouts.app')
@section('content')
    @if (session('status'))
        <div class="alert alert-success">
            {{ session('status') }}
        </div>
    @endif
    <div class="row">
        <div class="col-12">
            <h1>Список proxy серверов</h1>
            <div class="form-group">
                <a href="#" class="btn btn-success" id="buttonAddProxy">
                    Добавить список proxy
                </a>
            </div>
            <div class="panel panel-default">
                <div class="panel-body">
                    <table class="table table-bordered table-striped">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Адрес</th>
                            <th>Статус</th>
                            <th width="100">&nbsp;</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($allProxys as $proxy)
                            <tr>
                                <td>{{$proxy->id}}</td>
                                <td>{{$proxy->address}}</td>
                                <td>{{$proxy->status}}</td>
                                <td>
                                    <a href="#" class="btn btn-xs btn-default addTagButton" data-toggle="modal" data-target="#myModals" data-idDomain="{{$proxy->id}}" data-nameDomain="{{$proxy->address}}" data-form="editTagForm">
                                        Редактировать
                                    </a>
                                    <a href="/test/proxy/delete/{{$proxy->id}}"
                                       class="btn btn-xs btn-danger">
                                        Удалить
                                    </a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection