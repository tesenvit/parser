@php
    $url = '';
    if(isset($feed->linksContent))
        $url = $feed->linksContent->url;
@endphp
<div id="load">
    <ul class="list_result_ul">
        @if(!empty($feeds))
            @foreach($feeds as $feed)
                <li>
                    <div class="col-8 tablet">
                        <div>
                            <p class="feed-title"> {{ $feed->title }}</p>
                        </div>
                        <div>
                            <a href="{{ $url }}" target="_blank" class="feed-url-on-original">
                                {{ parse_url($url, PHP_URL_PATH) }}
                            </a>
                        </div>
                        <div class="list_result_ul_chanel">
                            <a id="list_result_ul_chanel"
                               href="{{ route('front.domains', str_replace('www.', '', parse_url($url, PHP_URL_HOST))) }}"
                               target="_blank">
                                {{ '@ '.str_replace('www.', '', parse_url($url, PHP_URL_HOST)) }}
                            </a>
                        </div>
                        <div>@if($feed->description != 1){{ $feed->description }}@endif</div>
                        <div>Дата публикации страницы {{ $feed->created_page }}</div>
                    </div>
                    <div class="col-4 tablet">
                        <a class="images_popup" href="{{ $feed->image }}">
                            <img src="{{ $feed->image ?? asset('images/default.png') }}" alt="">
                        </a>
                    </div>
                    <div class="col-12" id="feed-tags">

                        @php
                            $tags = $feed->tags->pluck('name')->toArray();
                            $tags = array_map(function ($v){
                                return mb_strtolower($v);
                            },$tags);
                        @endphp

                        @foreach (array_unique($tags) as $tag)
                            {{--<a href="/?search={{$tag->tag}}&typeSearch=search_name">--}}
                            <span class="tags-link">{{$tag}}</span>
                            {{--</a>--}}
                        @endforeach
                    </div>
                </li>
            @endforeach

        @endif
    </ul>
    {{ $feeds->links('paginate_one') }}
</div>