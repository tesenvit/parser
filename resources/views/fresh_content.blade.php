@foreach($freshContents as $content)
    <li class="list-group-item">

        <div class="row list-group-item-row">
            <div class="col-lg-2 text-center">
                @if ($content->image)
                    <img class="rounded-circle fresh-content-img" src="{{ $content->image ?? '' }}" alt="">
                @else
                    <img class="rounded-circle fresh-content-img" src="https://placehold.it/45x45" alt="">
                @endif
            </div>

            <div class="col-lg-9">
                <div class="media-body">

                    <a href="{{ $content->linksContent->url ?? '' }}" target="_blank">
                        <strong>{{$content->title}}</strong>
                    </a>

                    <div>{{$content->description}}</div>

                    <div class="fresh-content-dates">
                        <div class="text-muted smaller">Дата разбора страницы: {{$content->created_at}}</div>
                        @if ($content->created_page)
                            <div class="text-muted smaller">Дата создания страницы: {{$content->created_page}}</div>
                        @else
                            <div class="text-muted smaller">Дата создания страницы не известна</div>
                        @endif
                    </div>

                </div>
            </div>

        </div>
    </li>
@endforeach