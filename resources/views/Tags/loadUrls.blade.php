<div id="load">
    <ul class="list_result_ul">
        @if(!empty($tags))
            @foreach($tags as $tag)
                <li>
                    <div class="col-8 tablet">
                        <div>
                            <a href="{{$tag->url}}" target="_blank">
                                {{ $tag->title }}
                            </a>
                        </div>
                        <div>
                            {{ parse_url($tag->url, PHP_URL_PATH) }}
                        </div>
                        <div>@if($tag->description != 1){{ $tag->description }}@endif</div>
                        <div>Дата публикации страницы {{ $tag->created_page }}</div>
                    </div>
                    <div class="col-4 tablet">
                        <a class="images_popup" href="{{ $tag->image }}">
                            <img src="{{ $tag->image }}" alt="">
                        </a>
                    </div>
                    <div class="col-12">
                        @foreach ( $tagsName->tags($tag->url_id) as $tag)
                            <a href="/?search={{$tag->tag}}&typeSearch=search_name">
                                                <span class="tags-link">
                                                    {{$tag->tag}}
                                                </span>
                            </a>
                        @endforeach
                    </div>
                </li>
            @endforeach
            {{ $tags->links('paginate_one') }}
        @endif
    </ul>
</div>