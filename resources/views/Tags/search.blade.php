@extends('layouts.app')

@section('content')
    <div class="container pageTags">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <h3 class="panel-heading textCenter">Популярные поисковые запросы</h3>
                    <div class="panel-body">
                        @if (session('status'))
                            <div class="alert alert-success">
                                {{ session('status') }}
                            </div>
                        @endif
                        <div class="row">
                            <div class="customForm col-lg-4">
                                <div class="input">
                                    <input type="text" name="searchTags" id="searchTags">
                                </div>
                            </div>
                            <div class="text-right pageTags__links-block">
                                <div><a href="{{ route('tags') }}?tab=name" class="btm_turquoise_right">Название</a>
                                </div>
                                <div><a href="{{ route('tags') }}?tab=new" class="btm_turquoise_right">Новые</a></div>
                            </div>
                        </div>
                        <div class="popular-search searchPageAjax textCenter">
                            @if (isset($allTags))
                                @include('Tags.loadSearch')
                            @else
                                <h3>результат поиска пустой</h3>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

