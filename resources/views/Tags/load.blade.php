<div id="load">
    @foreach($allTags as $allTag)
        <a href="{{ route('tags.urls', $allTag->id) }}" class="magnific tags-link" data-id="{{$allTag->tag}}">
            {{$allTag->tag}} ({{$allTag->count}})
        </a>
    @endforeach
    @if (request()->query('name_tags'))
        {{ $allTags->appends(['name_tags' => request()->query('name_tags')])->links('paginate_one') }}
    @else
        {{ $allTags->links('paginate_one') }}
    @endif
    <div id="tags-link"
         class="col-sm-7 col-md-7 col-xs-12 col-lg-4 panel panel-default modal-block-center white-popup mfp-hide "></div>
</div>