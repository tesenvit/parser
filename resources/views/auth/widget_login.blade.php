<div class="modal fade" id="login-with-social" tabindex="-1" role="dialog" aria-labelledby="login-with-social-label" aria-hidden="true">
    <div class="modal-dialog modal-md" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p class="text-center modal-auth-title">Авторизация</p>

                <div class="row text-center">

                    <div class="col-md-8 col-xs-10 col-md-offset-2 col-xs-offset-1">
                        <a href="{!! route('socialite.auth', 'facebook') !!}" class="btn btn-md btn-block modal-facebook">
                            <i class="fab fa-facebook-f"></i>
                            &nbsp;
                            <span class="modal-facebook-text">Войти с помощью Facebook</span>
                        </a>
                    </div>

                    <div class="col-md-8 col-xs-10 col-md-offset-2 col-xs-offset-1">
                        <a href="{!! route('socialite.auth', 'google') !!}" class="btn btn-md btn-block modal-google">
                            <i class="fab fa-google-plus-g"></i>
                            &nbsp;
                            <span class="modal-google-text">Войти с помощью Google</span>
                        </a>
                    </div>

                </div>

            </div>
        </div>
    </div>
</div>