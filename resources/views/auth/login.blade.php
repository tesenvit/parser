@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">

        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default panel-auth-google">
                <div class="panel-heading">

                    <div class="row">
                        <div class="col-md-3 col-xs-12">
                            <p class="h4">Войти с помощью:</p>
                        </div>
                        <div class="col-md-3 col-xs-12 socialite-btn-box">
                            <a href="{!! route('socialite.auth', 'google') !!}" class="go__google">
                                <i class="fab fa-google"></i>
                                <span class="google-text"><strong>Google</strong></span>
                            </a>
                        </div>
                        <div class="col-md-3 col-xs-12 socialite-btn-box">
                            <a href="{!! route('socialite.auth', 'facebook') !!}" class="go__facebook">
                                <i class="fab fa-facebook"></i>
                                <span class="facebook-text"><strong>Facebook</strong></span>
                            </a>
                        </div>
                    </div>

                </div>
            </div>
        </div>

    </div>
</div>
@endsection
