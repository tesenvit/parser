<div id="load">
    <ul class="list_result_ul">
        @if(!empty($feeds))
            @foreach($feeds as $feed)
                <li>
                    <div class="col-8">
                        <div>
                            <a href="{{$feed->linksContent['url']}}" target="_blank">
                                {{ $feed->title }}
                            </a>
                        </div>
                        <div>
                            {{ parse_url($feed->linksContent['url'], PHP_URL_PATH) }}
                        </div>
                        <div>@if($feed->description != 1){{ $feed->description }}@endif</div>
                        <div>Дата публикации страницы {{ $feed->created_at }}</div>
                    </div>
                    <div class="col-4">
                        <a class="images_popup" href="{{ $feed->image }}">
                            <img src="{{ $feed->image }}" alt="">
                        </a>
                    </div>
                    <div class="col-12">
                        @php
                            $tags = $feed->tags->pluck('name')->toArray();
                            $tags = array_map(function ($v){
                                return mb_strtolower($v);
                            },$tags);
                        @endphp

                        @foreach (array_unique($tags) as $tag)
                            {{--<a href="/?search={{$tag->tag}}&typeSearch=search_name">--}}
                            <span class="tags-link">{{$tag}}</span>
                            {{--</a>--}}
                        @endforeach
                    </div>
                </li>
            @endforeach
            {{ $feeds->links('paginate_one') }}
        @endif
    </ul>
</div>
