@foreach($organizers as $organizer)
    <li class="list-group-item list-domains-item">

        <div class="row organizer">

            <div class="col-md-1 col-xs-1 text-center">
                <div class="organizer-favicon">
                    @if($organizer->website)
                        <img src="https://www.google.com/s2/favicons?domain={{ $organizer->website }}" alt="favicon">
                    @else
                        <img src="{{ asset('images/default-favicon.png') }}" alt="favicon">
                    @endif
                </div>
            </div>

            <div class="col-md-9 col-xs-8">

                <p class="organizer-name">{{ $organizer->name }}</p>

                <a class="btn btn-facebook-organizer" href="https://www.facebook.com/{{ $organizer->fb_group_id }}/" target="_blank">
                    <span class="btn-facebook-organizer-text">Подробнее</span>
                    <i class="fab fa-facebook-f btn-facebook-organizer-icon"></i>
                </a>

                <p class="organizer-body">{{ $organizer->getFormattedBody() }}</p>

                @if($organizer->manualTags)
                    <div style="margin-top: 10px;margin-bottom: 8px">
                        @foreach($organizer->manualTags as $tag)
                            <a href="{{ route('main.page', ['search' =>  $tag->tag, 'typeSearch' => 'search_name']) }}" target="_blank">
                                <span class="tags-link">
                                    <strong>{{ $tag->tag }}</strong>
                                </span>
                            </a>
                        @endforeach
                    </div>
                @endif

                <div class="btn-group organizers-subscribe-box">

                    <div class="btn-group organizer-user-subscribe-content">
                        @php
                            $checkSubscribe = Auth::check() && Auth::user()->checkOrganizerSubscribe($organizer->id);
                        @endphp

                        <button type="button" class="btn btn-default dropdown-toggle btn-sm organizer-subscribe-btn" data-toggle="dropdown" data-organizer-id="{{ $organizer->id }}">
                            <i class="fas fa-rss organizer-bookmark-subscribe {{ $checkSubscribe ? 'organizer-bookmark-subscribe-saved' : '' }}"></i>
                            <span><strong class="organizer-subscribe-btn-text">{{ $checkSubscribe ? 'Подписан' : 'Подписаться' }}</strong></span>
                            <span class="caret organizer-caret-subscribe"></span>
                        </button>

                        <ul class="dropdown-menu organizer-subscribe-dropdown">
                            <li>
                                @if($checkSubscribe)
                                    <span class="organizer-unsubscribe" data-organizer-id="{{ $organizer->id }}">Отменить подписку на эту страницу</span>
                                @else
                                    <span class="organizer-subscribe" data-organizer-id="{{ $organizer->id }}">Подписаться на эту страницу</span>
                                @endif
                            </li>
                            <li><a href="#">Посмотреть все подписки</a></li>
                        </ul>
                    </div>

                    <div class="btn-group organizer-user-save-content">
                        @include('Organizer.user_save_content')
                    </div>

                    <button type="button" class="btn btn-default btn-sm">
                        <i class="fas fa-ellipsis-h organizer-ellipsis-subscribe"></i>
                    </button>

                </div>
            </div>

            <div class="col-md-2 col-xs-3 like-box" data-organizer-id="{{ $organizer->id }}">
                <div class="panel panel-default text-center">
                    <div class="panel-body">
                        <div class="subscribe-box-body">
                            <i class="fas fa-caret-up fa-lg caret-icon"></i>
                            <p>
                                <strong class="organizer-likes-count">{{ $organizer->users_like_count }}</strong>
                            </p>
                            <div class="heart-subscribe">
                                @if(Auth::check() && Auth::user()->checkOrganizerLike($organizer->id))
                                    <i class="fas fa-heart filled-heart-icon"></i>
                                @else
                                    <i class="far fa-heart empty-heart-icon"></i>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </li>
@endforeach