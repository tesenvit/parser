<div id="load">
    <ul class="list_result_ul">
        @foreach ($news as $new)
            <li class=" @if (!$new->title)no-content @endif">
                <div class="col-8 table">
                    @if ($new->title)
                        <div>
                            <a href="{{ $new->url }}">{{ $new->title }}</a>
                        </div>
                        <div>{{ $new->url }}</div>
                        <div>{{ $new->description }}</div>
                        @if ($new->created_page)
                            <div>Дата публикации страницы
                                {{ $new->created_page }}
                            </div>
                        @endif
                    @else
                        <div>Контент еще не готов</div>
                        <div>{{ $new->url }}</div>
                    @endif
                </div>
                <div class="col-4 tablet">
                    <a class="images_popup" href="{{ $new->image }}">
                        <img src="{{ $new->image }}" alt="">
                    </a>
                </div>
            </li>
        @endforeach
        {{ $news->links('paginate_one') }}
    </ul>
</div>