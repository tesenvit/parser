<?
/**
 * Start deamon in console command /usr/bin/php /home/unni/public_html/public/deamon.php & >/dev/null
 */
$stop = false;
/**
 * pcntl_fork() - данная функция разветвляет текущий процесс
 */
$pid = pcntl_fork();
if ($pid == -1) {
    /**
     * Не получилось сделать форк процесса, о чем сообщим в консоль
     */
    die('Error fork process' . PHP_EOL);
} elseif ($pid) {
    /**
     * В эту ветку зайдет только родительский процесс, который мы убиваем и сообщаем об этом в консоль
     */
    die('Die parent process' . PHP_EOL);
} else {
    /**
     * Бесконечный цикл
     */
    while(!$stop) {
        for ($i = 0; $i < 100000000; $i++) {
            $command = 'wget https://unni.io/auditorbotlist';
            exec($command, $op);
            $command = 'wget https://unni.io/auditorbots';
            exec($command, $op);
            $command = 'wget https://unni.io/jewelerbot';
            exec($command, $op);
            $command = 'wget https://unni.io/jewelerbot/tags';
            exec($command, $op);
        }
        //Завершим корректно выполнение демона
        $stop = true;
    }
}
/**
 * Установим дочерний процесс основным, это необходимо для создания процессов
 */
posix_setsid();
?>