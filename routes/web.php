<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
$adminURL = env('APP_ADMIN_URL', 'test');
//Route::get('/', function () {
//    return view('welcome');
//});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::post('/navlogin/referer', 'Auth\AuthController@saveRefererToSession');

////auditor bot NEWS
Route::get('/auditorbots', 'Bots\Auditor\AuditorUrlsController@listDomain')->name('listDomain');
Route::get('/auditorbotlist', 'Bots\Auditor\AuditorUrlsController@list_url')->name('auditorbotlist');
Route::get('/twoturn', 'Bots\Auditor\AuditorTwoTurnController@index')->name('twoturn');


Route::get('/test_url', 'Bots\Auditor\AuditorUrlsController@test_url')->name('test.url');


//Route::get('/auditorlist', 'Bots\Auditor\AuditorUrlsController@startUrl')->name('startUrl');
//
Route::get('/startparsurls/{domain}', 'Bots\Auditor\AuditorUrlsController@startParseUrls')->name('startParsUrls');
Route::get('/tableTmp', 'Bots\Auditor\AuditorUrlsController@tableTmp')->name('tableTmp');
//
////JewelerBot
Route::get('/jewelerbot', 'Bots\JewelerBot\JewelerController@analysisContent')->name('jewelerbot');
Route::get('/jewelerbot/tags', 'Bots\JewelerBot\JewelerController@analysisTags')->name('analysisTags');
Route::get('/jewelerbot/search-links', 'Bots\JewelerBot\JewelerController@analysisLiks')->name('analysisLiks');
//Route::get('/jewelerbot/tags/delete', 'Bots\JewelerBot\JewelerController@deleteContent')->name('analysisTagsdd');
//Route::get('/jewelerbot/elasticIndexisContent', 'Bots\ElasticIndex\ElasticMetaController@elasticIndexisContent')->name('analysisContentUrl');
//
////auditor bot
//Route::get('/auditor_bot', 'AuditorBot\AuditorController@index')->name('index');
//Route::get('/auditor_bot/clear_links', 'AuditorBot\AuditorController@clearLinks')->name('index');
////miner bot
//Route::get('/miner_bot', 'MinerBot\MinerController@index')->name('index');
////miner bot NEWS
//Route::get('/miner_bot_tags', 'Bots\Miner\MinerContentController@parseMetaTags')->name('parseMetaTags');
//Route::get('/miner_bot_tags_news', 'Bots\Miner\MinerContentController@parseMetaTagsNew')->name('parseMetaTagsNew');
//Route::get('/miner_bot_tags_newss', 'Bots\Miner\MinerContentController@parseMetaTagsNew')->name('parseMetaTagsNew');

//Admin middleware
Route::group(
    [
        'prefix' => $adminURL,
        'namespace' => 'Admin',
        'as' => 'admin.',
        'middleware' => ["admin","auth"],
    ],
    function(){
        Route::get('/test_link',['as' => 'dashboard','uses' => 'DashboardController@testing']);
        Route::get('/',['as' => 'dashboard','uses' => 'DashboardController@index']);
        Route::get('/fresh_content',['as' => 'dashboard','uses' => 'DashboardController@freshContent']);
        Route::post('/ajax/stat', 'DashboardController@statistics');
        /**
         * Routs Domains
         */
//        Route::resource('/domains', 'DomainsController');
        Route::resource('/domains', 'DomainsController');
        Route::get('/domains/type/{type}', 'DomainsController@type')->name('domains.type');
        Route::post('/domains/error-parser', 'DomainsController@error_parser')->name('domains.error.parser');
        Route::post('/domains/tag/delete', 'DomainsController@tagDelete');
        Route::post('/domains_enable',['as' => 'dashboard','uses' => 'DomainsController@onOffDomains']);

        /**
         * Routs Tags
         */
        Route::get('/tags',['as' => 'dashboard','uses' => 'TagsController@index']);
        Route::get('/tags/add_tags', 'TagsController@addTag')->name('tags.add_tags');
        Route::post('/tags/add_tags',['as' => 'dashboard','uses' => 'TagsController@addTag']);
        Route::get('/tags/delete/{id}',['as' => 'dashboard','uses' => 'TagsController@delete']);
        Route::post('/tags/get', 'TagsController@getTags')->name('tags.get');
        Route::post('/tags/show_switch_active', 'TagsController@showSwitchActive')->name('tags.show.switch.active');
        Route::post('/tags/search', 'TagsController@search')->name('tags.search');
        /**
         * Routs Proxy
         */
        Route::get('/proxy',['as' => 'dashboard','uses' => 'ProxyController@index']);
        Route::get('/proxy/addProxyForm',['as' => 'dashboard','uses' => 'ProxyController@forms']);
        Route::post('/proxy/addProxy',['as' => 'dashboard','uses' => 'ProxyController@formsSend']);
        /**
         * Routs hint with dictionary
         */
        Route::get('/hint_with_dictionary',['as' => 'dashboard','uses' => 'DictionaryController@index']);
        Route::get('/hint_with_dictionary/add',['as' => 'dashboard','uses' => 'DictionaryController@create']);
        Route::post('/hint_with_dictionary/add',['as' => 'dashboard','uses' => 'DictionaryController@create']);
        Route::get('/hint_with_dictionary/add_file',['as' => 'dashboard','uses' => 'DictionaryController@create_file']);
        Route::post('/hint_with_dictionary/add_file',['as' => 'dashboard','uses' => 'DictionaryController@create_file']);
        /**
         * Routs search history
         */
        Route::get('/search_history',['as' => 'dashboard','uses' => 'SearchHistoryController@index']);

        /**
         * Routs group pages
         */
        Route::get('/pages',['as' => 'dashboard','uses' => 'PagesController@index']);
        Route::group(['prefix' => 'pages'], function () {
            Route::get('/create',['as' => 'dashboard','uses' => 'PagesController@create']);
            Route::post('/store',['as' => 'dashboard','uses' => 'PagesController@store']);
            Route::get('/destroy/{id}',['as' => 'dashboard','uses' => 'PagesController@destroy']);
            Route::get('/edit/{id}',['as' => 'dashboard','uses' => 'PagesController@edit']);
            Route::post('/edit/{id}',['as' => 'dashboard','uses' => 'PagesController@update']);

        });
        Route::get('/demons',['as' => 'dashboard','uses' => 'DemonsController@index']);
        Route::group(['prefix' => 'demons'], function () {
            Route::get('/create',['as' => 'dashboard','uses' => 'DemonsController@create']);
            Route::post('/store',['as' => 'dashboard','uses' => 'DemonsController@store']);
            Route::get('/destroy/{id}-{id_pid}',['as' => 'dashboard','uses' => 'DemonsController@destroy']);

        });
        //Routes settings
        Route::group(['prefix' => 'settings'], function () {
            Route::get('/paser',['as' => 'dashboard','uses' => 'SettingsParserController@index']);
            Route::post('/paser/delete',['as' => 'dashboard','uses' => 'SettingsParserController@delete']);
            Route::get('/paser/create',['as' => 'dashboard','uses' => 'SettingsParserController@create']);
            Route::post('/paser/create',['as' => 'dashboard','uses' => 'SettingsParserController@store']);
            Route::post('/paser/stopstart',['as' => 'dashboard','uses' => 'SettingsParserController@stopstart']);
            Route::post('/paser/setting',['as' => 'dashboard','uses' => 'SettingsParserController@setting']);
            Route::get('/paser/status',['as' => 'dashboard','uses' => 'SettingsParserController@ajax_status_parser']);

            Route::get('/tags', 'SettingsParserController@tags')->name('settings.tags');
            Route::post('/tags', 'SettingsParserController@tagsPost')->name('settings.tagsPost');
        });
        //Routes organizers
        Route::resource('/organizers', 'OrganizerController');
        Route::post('/organizers/loading_city', 'OrganizerController@loading_city')->name('organizers.loading.city');

        //Routes statistics
        Route::get('/statistics', 'StatisticsController@index')->name('statistics');
        Route::get('/statistics/tags', 'StatisticsController@tags')->name('statistics.tags');
        Route::post('/statistics/tags/chart_new', 'StatisticsController@chartNewTags')->name('statistics.tags.chart.new');
        Route::get('/statistics/domains', 'StatisticsController@domains')->name('statistics.domains');
        Route::get('/statistics/domains/params', 'StatisticsController@domainsParams')->name('statistics.domains.params');

        Route::get('/statistics/click', 'StatisticsController@clickLinks')->name('statistics.click.links');

        //Routes parser logs
        Route::get('/statistics/parser_logs', 'ParserLogController@index')->name('statistics.parser.logs');
        Route::get('/statistics/parser_logs/{id}', 'ParserLogController@showProcessLogs')->name('statistics.parser.logs.process');
        Route::post('/statistics/parser_logs/switch_active', 'ParserLogController@switchActive')->name('statistics.parser.logs.switch.active');

        /**
         * Routs Seo
         *
         */
        Route::resource('seo', 'SeoController');
        Route::post('/seo/switch_active', 'SeoController@switchActive')->name('seo.switch.active');
    });

/**
 * rounts users front
 */
//Live search
Route::get('/live_search', 'HomePageController@liveSearch')->name('index');

Route::get('/', 'Front\SearchContentController@search')->name('main.page');
Route::post('/highlighting', 'Front\SearchContentController@highlighting');

Route::post('/tips', 'HomePageController@tips');

Route::get('/tags', 'Front\TagsController@index')->name('tags');
Route::post('/tags', 'Front\TagsController@index')->name('tags');
Route::post('/tags/add-user', 'Front\TagsController@addUser')->name('tags.add-user');
Route::post('/tags', 'Front\TagsController@index');
Route::get('/tags/urls/{tags}', 'Front\TagsController@urls')->name('tags.urls');

Route::get('/pages/{slug}', 'Front\PagesController@page');

Route::get('/searches', 'Front\SearchContentController@testSearch');

// Front pages
/*
 * feeds pages
 */
Route::get('/feeds', 'Front\FeedsController@index')->name('front.feeds');
Route::get('/archive', 'Front\FeedsController@archive_feeds');

Route::get('/profile/site/add', 'Front\ProfileController@siteAdd')->name('front.profile.site.add');
Route::post('/profile/site/store', 'Front\ProfileController@store')->name('front.profile.site.store');

//ajax request front
//add user tags
Route::get('/offer-tags', function (){
    if(\request()->ajax()){
        return view('Domains.forms.add_user_tags');
    } else {
        abort(404);
    }
})->name('offer-tags');

/*
 * domains pages
 */
Route::get('/domains/{name_domains}', 'Front\DomainsController@domain')->name('front.domains');
Route::get('/domains/{name_domains}/tags', 'Front\DomainsController@domainTags')->name('front.domains.tags');
/*
 * organizers pages
 */
Route::get('/organizers', 'Front\OrganizerController@index')->name('front.organizers');
Route::get('/organizers/add', 'Front\OrganizerController@add')->name('organizers.add');
Route::post('/organizers/add', 'Front\OrganizerController@add')->name('organizers.add');

Route::post('/organizers/get-liked-users', 'Front\OrganizerController@getLikeUsers')->name('organizers.add');
Route::post('/organizers/like', 'Front\OrganizerController@markAsLiked')->name('organizers.mark_as_liked');

Route::post('/organizers/user-save', 'Front\OrganizerController@userSave')->name('organizers.user_save');

Route::post('/organizers/user-subscribe', 'Front\OrganizerController@userSubscribe')->name('organizers.user_subscribe');

Route::post('/organizers', 'Front\OrganizerController@savingStateSortingInSession')->name('front.organizers.sort');

/*
 * news pages
 */
Route::get('/news', 'Front\NewsController@index')->name('news');
/*
 * save click remote link on user
 */
Route::post('/follow/click', 'Front\UserFixedController@clickRemoteLinks')->name('follow.click');
Route::post('/follow/channel', 'Front\UserFixedController@followChannel')->name('follow.channel');
/*
 * Auth user social
 */
Route::get(
    '/socialite/{provider}',
    [
        'as' => 'socialite.auth',
        function ( $provider ) {
            return \Socialite::driver( $provider )->redirect();
        }
    ]
);
Route::get('login/{provider}/callback', 'Auth\LoginController@handleProviderCallback');
//Route::get('/login/{provider}/callback', function ($provider) {
//    $user = \Socialite::driver($provider)->user();
//});
Route::get('auth/logout', 'Auth\AuthController@getLogout')->name('user.logout');

Route::post('/front_fresh_content', 'Front\SearchContentController@getFreshContent')->name('front.fresh.content');

Route::get('custom-test', 'TestController@🔧🔨⚒');