<?php
/**
 * Created by PhpStorm.
 * User: Des-2
 * Date: 06.06.2019
 * Time: 16:27
 */

namespace App\Components\ParserLogger\Models;


use Illuminate\Database\Eloquent\Model;

class ParserLoggerCycle extends Model
{
    const UPDATED_AT = null;

    protected $table = 'parser_logger_cycles';

    public function processManager()
    {
        return $this->belongsTo(ParserProcessManager::class, 'processes_id', 'id');
    }

    public function processes()
    {
        return $this->hasMany(ParserLoggerCycleProcess::class, 'cycles_id', 'id');
    }
}