<?php
/**
 * Created by PhpStorm.
 * User: Des-2
 * Date: 06.06.2019
 * Time: 16:59
 */

namespace App\Components\ParserLogger;


use App\Components\ParserLogger\Models\ParserLoggerCycleProcess;
use Illuminate\Support\Facades\Facade;

/**
 * Class ParserLoggerFacade
 * @package App\Components\ParserLogger
 *
 * @method static int startProcess(string $name)
 * @method static void stopProcess($processId = null, $comment)
 * @method static ParserLoggerCycleProcess getProcess($processId)
 * @method static offAndRemove()
 * @method static loadCycle($cycleId)
 */
class ParserLoggerFacade extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'parser_logger';
    }
}