<?php

namespace App\Components\Analyzer;

use App\Exceptions\UnknownFormat;
use App\Facades\Analyzer as FacadeAnalyzer;
use App\Jobs\StoreUrlsToSitemap;
use Illuminate\Support\Facades\Log;
use XMLReader;

class Sitemap extends Analyzer
{
    /**
     * Resource of XMLReader().
     */
    private $reader;

    /**
     * Tags that we collect in the sitemap.
     */
    private $requiredTags = [
        'loc',
        'lastmod',
        'changefreq',
        'priority'
    ];

    /**
     * Temporary repository for URL.
     */
    private $tmpRepositoryUrlsData = [];

    /**
     * Number of pieces to write to the database.
     */
    private $chunkSize = 1000;

    /**
     * Pointer for parsing GZip file.
     */
    private $urlFlagForGZ;

    /**
     * Count the number of URLs.
     *
     * @param string $sitemap
     * @throws UnknownFormat
     *
     * @return int
     */
    public function countUrls(string $sitemap) :int
    {
        $extension = $this->getExtension($sitemap);

        switch ($extension)
        {
            case 'xml':
                return (int)$this->countUrlFromXML($sitemap);
                break;

            case 'gz':
                return (int)$this->countUrlFromGZ($sitemap);
                break;

            default:
                throw new UnknownFormat('Impossible counted urls from sitemap', $extension);
        }
    }

    /**
     * Get all sitemaps of domen.
     *
     * @param string $url
     *
     * @return array
     */
    public function getSitemaps(string $url): array
    {
        $sitemaps = [];

        $urlSitemaps = $this->makeUrl($url);

        foreach ($urlSitemaps as $url)
        {
            try
            {
                $this->makeXmlReader($url);

                if($this->httpStatusCode === 200)
                {
                    // <urlset> </urlset>
                    while($this->reader->read())
                    {
                        // <url> </url>
                        if ($this->checkStartTagByName('url'))
                        {
                            $sitemaps[] = $url;
                            continue 2;
                        }

                        // <sitemap> </sitemap>
                        if ($this->checkStartTagByName('sitemap'))
                        {
                            while($this->reader->read())
                            {
                                // <loc> </loc>
                                if ($this->checkStartTagByName('loc'))
                                {
                                    $this->reader->read();

                                    // link,  ༒ miakotka ༒
                                    if ($this->checkItemAsTextFromReader())
                                        $sitemaps[] = $this->reader->value;

                                    break;
                                }
                            }
                        }
                    }
                }

                if($this->httpStatusCode === 301)
                {
                    $httpsUrl = str_replace('http://','https://', $url);

                    if($httpsUrl !== $url)
                        $sitemaps = array_merge($sitemaps, $this->getSitemaps($httpsUrl));
                    else
                        continue;
                }
            }
            catch (\Exception $e)
            {
                $this->writeToLog($e, $url);
                continue;
            }
        }

        return array_unique($sitemaps);
    }

    /**
     * Write URL to database regardless of sitemap extension.
     *
     * @param object $sitemap
     *
     * @return void
     */
    public function storeUrls(object $sitemap) :void
    {
        if($this->getExtension($sitemap->url) === 'xml')
            $this->storeUrlsForXml($sitemap);
        
        if($this->getExtension($sitemap->url) === 'gz')
            $this->storeUrlsForGZ($sitemap);
    }

    /**
     * Parse links in sitemap with XML extension and write to the database in a separate job queue
     *
     * @param object $sitemap
     *
     * @return void
     */
    private function storeUrlsForXml(object $sitemap) :void
    {
        try
        {
            $urls = [];

            $this->makeXmlReader($sitemap->url);

            if($this->httpStatusCode === 200)
            {
                while($this->reader->read())
                {
                    if ($this->checkStartTagByName('url'))
                    {
                        while($this->reader->read())
                        {
                            if ($this->reader->localName === 'url') break;
                            $this->storeRequiredTagsForXML($sitemap->id);
                        }

                        $urls[] = $this->tmpRepositoryUrlsData;

                        if(count($urls) === $this->chunkSize)
                        {
                            dispatch(new StoreUrlsToSitemap($urls));
                            $urls = [];
                        }
                    }
                }

                dispatch(new StoreUrlsToSitemap($urls));
            }
        }
        catch (\Exception $e)
        {
            $this->writeToLog($e, $sitemap->url);
        }
    }

    /**
     * Collect an array of URL data for writing to the database for XML files.
     *
     * @param int $sitemapId
     *
     * @return void
     */
    private function storeRequiredTagsForXML(int $sitemapId) :void
    {
        for($i = 0; count($this->requiredTags) > $i; $i++)
        {
            if($this->checkStartTagByName($this->requiredTags[$i]))
            {
                $this->reader->read();

                if ($this->checkItemAsTextFromReader())
                    $this->tmpRepositoryUrlsData[$this->requiredTags[$i]] = $this->reader->value;
            }
        }

        $this->tmpRepositoryUrlsData['sitemap_id'] = $sitemapId;
        $this->tmpRepositoryUrlsData['created_at'] = now();
        $this->tmpRepositoryUrlsData['updated_at'] = now();
    }

    /**
     * Parse links in sitemap with GZip extension and write to the database in a separate job queue
     *
     * @param object $sitemap
     *
     * @return void
     */
    private function storeUrlsForGZ(object $sitemap) :void
    {
        try
        {
            $gzFile = @fopen("compress.zlib://" . $sitemap->url, "r");

            $urls = [];

            while (($line = @fgets($gzFile)) != false)
            {
                if(preg_match('~<url>~', $line))
                {
                    $this->urlFlagForGZ = true;
                    continue;
                }

                if(preg_match('~<\/url>~', $line))
                {
                    $this->urlFlagForGZ = false;
                    $urls[] = $this->tmpRepositoryUrlsData;
                    continue;
                }

                if($this->urlFlagForGZ)
                    $this->storeRequiredTagsForGZ($line, $sitemap->id);

                if(count($urls) === $this->chunkSize)
                {
                    dispatch(new StoreUrlsToSitemap($urls));
                    $urls = [];
                }
            }

            dispatch(new StoreUrlsToSitemap($urls));
        }
        catch (\Exception $e)
        {
            $this->writeToLog($e, $sitemap->url);
        }
    }

    /**
     * Collect an array of URL data for writing to the database for GZip files.
     *
     * @param string $line
     * @param int $sitemapId
     *
     * @return void
     */
    private function storeRequiredTagsForGZ(string $line, int $sitemapId) :void
    {
        for($i = 0; count($this->requiredTags) > $i; $i++)
        {
            $ptrn = '~<' . $this->requiredTags[$i] . '>\s{0,5}(?<' . $this->requiredTags[$i] . '>.*?)<\/' . $this->requiredTags[$i] . '>~m';
            if(preg_match($ptrn, $line, $matches))
            {
                if (isset($matches[$this->requiredTags[$i]]))
                    $this->tmpRepositoryUrlsData[$this->requiredTags[$i]] = $matches[$this->requiredTags[$i]];
            }
        }

        $this->tmpRepositoryUrlsData['sitemap_id'] = $sitemapId;
        $this->tmpRepositoryUrlsData['created_at'] = now();
        $this->tmpRepositoryUrlsData['updated_at'] = now();
    }

    /**
     * Count the number of URLs for GZIP format.
     *
     * Since the file is read line by line, we introduce two variables:
     * $tagUrl - check if we are in the body of the <url> tag;
     * $otherTag - check if we found in the body of the <url> tag what we need, or is it some other;
     *
     * @param string $sitemap
     *
     * @return int
     */
    private function countUrlFromGZ(string $sitemap) :int
    {
        try
        {
            $counter = 0;

            $gzFile = @fopen("compress.zlib://" . $sitemap, "r");

            $inUrlFlag = false;

            while (($line = fgets($gzFile)) != false)
            {
                if(preg_match('~<url>$~', $line))
                    $inUrlFlag = true;

                if($inUrlFlag && preg_match('~<loc>\s{0,5}(?<url>https?:\/\/.*?)<\/loc>~m', $line, $matches))
                {
                    if(isset($matches['url']))
                    {
                        $counter++;
                        $inUrlFlag = false;
                    }
                }

            }
        }
        catch (\Exception $e)
        {
            $this->writeToLog($e, $sitemap);
        }

        return (int)$counter;
    }

    /**
     * Count the number of URLs for XML format
     *
     * @param string $sitemap
     *
     * @return int
     */
    private function countUrlFromXML(string $sitemap) :int
    {
        $counter = 0;

        try
        {
            $this->makeXmlReader($sitemap);

            if($this->httpStatusCode === 200)
            {
                while($this->reader->read())
                {
                    if ($this->checkStartTagByName('url'))
                    {
                        while($this->reader->read())
                        {
                            if ($this->checkStartTagByName('loc'))
                            {
                                $this->reader->read();

                                if ($this->checkItemAsTextFromReader())// ༒ miakotka ༒
                                    $counter++;

                                break;
                            }
                        }
                    }
                }
            }
        }
        catch (\Exception $e)
        {
            $this->writeToLog($e, $sitemap);
        }

        return (int)$counter;
    }

    /**
     *  Check type TEXT of nodetype
     *
     * @return bool
     */
    private function checkItemAsTextFromReader() :bool
    {
        return $this->reader->nodeType == XMLReader::TEXT;
    }

    /**
     * Check name of nodetype
     *
     * @param string $name
     *
     * @return bool
     */
    private function checkStartTagByName(string $name) :bool
    {
        return ($this->reader->nodeType == XMLReader::ELEMENT) && ($this->reader->localName === $name);
    }

    /**
     * Make XML Reader with HTTP code
     *
     * @param string $url
     *
     * @return void
     */
    private function makeXmlReader(string $url) :void
    {
        $reader = new XMLReader();
        $reader->open($url);

        $this->makeHttpStatusCode($url);

        $this->reader = $reader;
    }

    /**
     * The URL does not always have the correct format, we will configure it.
     * If we can’t find the path to the site map in the robots.txt file, add “sitemap.xml” at the end.
     *
     * @param string $url
     *
     * @return array|mixed
     */
    protected function makeUrl(string $url) :array
    {
        $urlSitemap = FacadeAnalyzer::robotTxt()->getPathsToSitemaps($url);

        if(!$urlSitemap)
        {
            if($this->checkHttp($url) || $this->checkSuitableExtension($url))
                $urlSitemap[] = ($this->checkSuitableExtension($url)) ? $url : $url . '/sitemap.xml';
            else
                $urlSitemap[] =  'http://' . $url . '/sitemap.xml';
        }

        return $urlSitemap;
    }

    /**
     * Check protocol of url
     *
     * @param string $url
     *
     * @return bool
     */
    private function checkHttp(string $url) :bool
    {
        return preg_match('/^(https?):\/\/.*/', $url);
    }

    /**
     * Check if it is a suitable data format.
     *
     * @param $url
     *
     * @return false|int
     */
    private function checkSuitableExtension(string $url) :bool
    {
        $ext = $this->getExtension($url);

        return ($ext === 'gz' || $ext === 'xml') ? true : false;
    }

    /**
     * Get extension
     *
     * @param string $url
     *
     * @return null|string
     */
    private function getExtension(string $url) :?string
    {
        $extension = null;

        if($url)
        {
            $document = @fopen($url, "r");

            if(preg_match('~^<\?xml~m', @fgets($document)))
            {
                $extension = 'xml';
            }
            else
            {
                $exploded = explode('.', $url);
                $extension = end($exploded);
            }
        }

        return $extension;
    }

    /**
     * Logging
     *
     * @param \Exception $e
     * @param string $url
     *
     * @return void
     */
    private function writeToLog(\Exception $e, string $url) :void
    {
        Log::debug('-------------------------------------------');
        Log::debug($url);
        Log::debug($e->getMessage());
        Log::debug('-------------------------------------------');
    }
}