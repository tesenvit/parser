<?php

namespace App\Components\Analyzer;

class AnalyzerFactory
{
    public function __call($name, $arguments)
    {
        $name = 'App\Components\Analyzer\\'.ucfirst($name);
        return new $name();
    }
}