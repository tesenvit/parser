<?php

namespace App\Http\Controllers\Front;

use App\Front\Domains;
use Curl\Curl;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ProfileController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function siteAdd()
    {
        return view('Profile.siteAdd');
    }
    public function store(Domains $domain)
    {
        $validator = Validator::make(\request()->all(), [
            'domain' => 'required|regex:^(?!\-)(?:[a-zA-Z\d\-]{0,62}[a-zA-Z\d]\.){1,126}(?!\d+)[a-zA-Z\d]{1,63}$^|unique:auditor_domain'
        ]);
        if ($validator->fails())
        {
            return redirect()->back()->withErrors($validator->errors())->withInput();
        }
        $curl = new Curl();
        if (!$curl->get('http://'.\request()->domain)){
            return redirect()->back()->withErrors('Домен не доступен')->withInput();
        }
        if (parse_url(\request()->domain, PHP_URL_HOST)){
            $url = parse_url(\request()->domain, PHP_URL_HOST);
        }else{
            $url = \request()->domain;
        }
        $count = $domain->count();

        $domain->domain = $url;
        $domain->priority = ++$count;
        $domain->type = 'user';
        $domain->save();
        return redirect()->route('front.profile.site.add')->with('status', 'Домен добавлен');
    }
}

