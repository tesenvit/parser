<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use App\Model\Fornt\Feed;
use Illuminate\Http\Request;

class FeedsController extends Controller
{
   public function index(Request $request)
   {
       $feeds = Feed::whereNotNull('created_page')->with(['linksContent','tags'])->paginate(25);

       if ($request->ajax())
           return view('Feeds.load', compact('feeds'))->render();

       return view('Feeds.index', compact('feeds'));
   }

   public function archive_feeds(Request $request)
   {
       $feeds = Feed::orderBy('id', 'desc')->with(['linksContent','tags'])->paginate(25);

       if ($request->ajax())
           return view('Archive.load', compact('feeds'))->render();

       return view('Archive.index', compact('feeds'));
   }
}
