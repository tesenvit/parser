<?php

namespace App\Http\Controllers\Front;

use App\Auditor\Auditor;
use App\Model\Bots\Auditor\AuditorUrls;
use App\Model\Fornt\AuditorUrl;
use App\Model\Fornt\Tag;
use App\Model\Tags;
use Elasticsearch\ClientBuilder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use JasonGrimes;
class TagsController extends Controller
{
    protected  $tags;

    public function __construct(Tags $tags)
    {
        $this->tags = $tags;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if (strlen($request->name_tags) > 2){
            $allTags = Tag::where('tag', 'LIKE', '%'.$request->name_tags.'%')
                ->orderBy('count', 'desc')
                ->paginate(100);
            if ($request->ajax()) {
                return view('Tags.load', ['allTags' => $allTags])->render();
            }
            return view('Tags.index', ['allTags' => $allTags]);
        }
        $allTags = $this->tags->orderBy('count', 'desc')->paginate(200);
        if ($request->tab && $request->tab == 'name'){
            $allTags = $this->tags->orderBy('tag', 'asc')->paginate(100);
        }
        if ($request->tab && $request->tab == 'new'){
            $allTags = $this->tags->orderBy('id', 'desc')->paginate(100);
        }
        if ($request->ajax()) {
            return view('Tags.load', ['allTags' => $allTags])->render();
        }
        return view('Tags.index', compact('allTags'));
    }
    public function scrollTags(Request $request){
        if (strlen($request->search) > 2){
            $resultSearch = Tag::where('tag', 'LIKE', '%'.$request->search.'%')
                ->orderBy('count', 'desc')
                ->paginate(100);
            return Response::json(view('Tags.search', ['resultSearch' => $resultSearch])->render());
        }
        return view('Tags.search');
    }
    public function searchTags(Request $request){
        $allTags = Tags::where('tag', 'LIKE', '%'.$request->search.'%')->paginate(10);
        return $allTags;
    }
    public function addUser(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'tag' => 'required|string',
        ]);
        if ($validator->fails())
        {
            return redirect()->back()->withErrors($validator->errors())->withInput();
        }
        $addStatus = Tags::updateOrCreate(
            ['url_id' => $request->id, 'tag' => $request->tag, 'type' => 'add_user']
        );
        if (empty($addStatus->wasRecentlyCreated)){
            return back()->with('status', 'Тег ранее был предложен');
        }
        return back()->with('status', 'Тег отправлен на модерацию');
    }
    public function urls(Request $request, $tagId)
    {
        $tagsName = Tag::where('id', $tagId)->first();
        $tags = DB::table('tags_urls')
            ->leftJoin('auditor_url_children', 'tags_urls.url_id', '=', 'auditor_url_children.id')
            ->leftJoin('jeweler_content', 'jeweler_content.id_url', '=', 'tags_urls.url_id')
            ->where('tag_id', $tagId)
            ->groupBy('tags_urls.url_id')
            ->orderBy('tags_urls.url_id', 'desc')
            ->paginate(25);
        if ($request->ajax()) {
            return view('Tags.loadUrls', ['tags' => $tags, 'tagsName' => $tagsName])->render();
        }
        return view('Tags.urls', ['tags' => $tags, 'tagsName' => $tagsName]);
    }
}
