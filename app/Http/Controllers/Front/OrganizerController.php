<?php

namespace App\Http\Controllers\Front;

use App\Front\Organizer;
use App\Front\OrganizerCross;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class OrganizerController extends Controller
{
    const FILTER_DATE_KEY = 'organizer_filter_date';
    const FILTER_OTHER_KEY = 'organizer_filter_other';
    const FILTER_TAG_KEY = 'organizer_filter_tag';

    public function index(Request $request)
    {
        $categories = $this->getCategories();

        $countOrganizersByDate = $this->getOrganizersListByDate();

        $page = 0;
        if ($request->has('page'))
            $page = $request->page;

        $organizersBuilder = $this->getOrganizersBySort();

        $organizers = $organizersBuilder->offset($page * 10)
                                        ->limit(10)
                                        ->withCount('usersLike')
                                        ->get();

        if ($request->ajax())
            return view('Organizer.list_organizers', compact('organizers', 'categories', 'countOrganizersByDate'))->render();

        return view('Organizer.index', compact('organizers', 'categories', 'countOrganizersByDate'));
    }

    public function getCategories()
    {
        return OrganizerCross::with(['tags', 'organizers'])
                            ->select('tags_id', \DB::raw('count(*) as count_tags'))
                            ->groupBy('tags_id')
                            ->orderBy('count_tags', 'desc')
                            ->limit(50)
                            ->get();
    }

    public function getFormattedNumber($number)
    {
        if ($number > 999)
            $number = round($number / 1000, 1) . 'K';

        return $number;
    }

    public function getOrganizersListByDate()
    {
        return [
            'today' => [
                'name' => 'Сегодня',
                'sort' => '-0 days',
                'count' => $this->getFormattedNumber($this->getOrganizersByDate('-0 days')->count())
            ],
            'yesterday' => [
                'name' => 'Вчера',
                'sort' => '-1 days',
                'count' => $this->getFormattedNumber($this->getOrganizersByDate('-1 days', '-1 days')->count())
            ],
            '3dayAgo' => [
                'name' => '3 дня',
                'sort' => '-3 days',
                'count' => $this->getFormattedNumber($this->getOrganizersByDate('-3 days')->count())
            ],
            '1weekAgo' => [
                'name' => 'Неделя',
                'sort' => '-7 days',
                'count' => $this->getFormattedNumber($this->getOrganizersByDate('-7 days')->count())
            ],
            '1monthAgo' => [
                'name' => 'Месяц',
                'sort' => '-1 month',
                'count' => $this->getFormattedNumber($this->getOrganizersByDate('-1 month')->count())
            ]
        ];
    }

    public function getOrganizersByDate($from = '-30 years', $to = '1 days')
    {
        $dateFrom = date("Y-m-d", strtotime($from));
        $dateTo = date('Y-m-d', strtotime($to));

        return Organizer::whereBetween('created_at', [$dateFrom, $dateTo]);
    }

    public function getOrganizersBySort()
    {
        $organizers = $this->getSortOrganizersByDate();

        $orderedOrganizers = $this->orderByWithFilter($organizers);

        $orderedOrganizersByTag = $this->orderedOrganizersByTag($orderedOrganizers);

        return $orderedOrganizersByTag;
    }

    public function orderedOrganizersByTag($organizers)
    {
        if($this->isTagsAll())
        {
            return $organizers;
        }
        else
        {
            $tagId = session(self::FILTER_TAG_KEY);

            return $organizers->whereHas('manualTags', function ($query) use ($tagId) {
                $query->where('tags_id', $tagId);
            });
        }
    }

    private function isTagsAll()
    {
        return (!session(self::FILTER_TAG_KEY) || session(self::FILTER_TAG_KEY) === 'all');
    }

    public function getSortOrganizersByDate()
    {
        $filterDateFrom = session(self::FILTER_DATE_KEY) ?? '-30 years';

        $filterDateTo = $this->isYesterday() ? '-1 days' : '1 days';

        return $this->getOrganizersByDate($filterDateFrom, $filterDateTo);
    }

    private function isYesterday(): bool
    {
        return session(self::FILTER_DATE_KEY) && session(self::FILTER_DATE_KEY) === '-1 days';
    }

    public function orderByWithFilter($organizers)
    {
        if (session(self::FILTER_OTHER_KEY) && session(self::FILTER_OTHER_KEY) === 'popularity')
            $orderedOrganizers = $organizers->withCount('usersLike')->orderBy('users_like_count', 'desc');
        else
            $orderedOrganizers = $organizers->orderBy('created_at', 'desc');

        return $orderedOrganizers;
    }

    public function add(Request $request)
    {
        if ($request->link)
        {
            $validator = Validator::make($request->all(), [
                'link' => 'required|string',
            ]);

            if ($validator->fails())
                return redirect()->back()->withErrors($validator->errors())->withInput();


            Organizer::create(['website' => $request->link, 'type' => 'user']);

            return back()->with('status', 'Организатор отправлен на проверку');
        }

        if (\request()->ajax())
            return view('Organizer.forms.add');
        else
            abort(404);
    }

    public function getLikeUsers(Request $request)
    {
        $users = Organizer::find($request->organizerId)
                          ->usersLike()
                          ->orderBy('created_at')
                          ->limit(20)
                          ->get();

        $usersContent = view('Organizer.modal_content', compact('users'))->render();

        return response()->json($usersContent);
    }

    public function userSave(Request $request)
    {
        if (Auth::check())
        {
            $organizer = Organizer::find($request->organizerId);

            if (Auth::user()->checkOrganizerSaved($organizer->id))
                Auth::user()->organizersSave()->detach($organizer->id);
            else
                Auth::user()->organizersSave()->attach([
                    'organizer_id' => $organizer->id
                ]);

            $response = view('Organizer.user_save_content', compact('organizer'))->render();

            return response()->json($response);
        }
    }

    public function userSubscribe(Request $request)
    {
        if (Auth::check())
        {
            $organizerId = $request->organizerId;

            if (Auth::user()->checkOrganizerSubscribe($organizerId))
                Auth::user()->organizersSubscribe()->detach($organizerId);
            else
                Auth::user()->organizersSubscribe()->attach([
                    'organizer_id' => $organizerId
                ]);
        }
    }

    public function markAsLiked(Request $request)
    {
        if (Auth::check())
        {
            $organizerId = $request->organizerId;

            if (Auth::user()->checkOrganizerLike($organizerId))
                Auth::user()->organizersLike()->detach($organizerId);
            else
                Auth::user()->organizersLike()->attach([
                    'organizer_id' => $organizerId
                ]);
        }
    }

    public function savingStateSortingInSession(Request $request)
    {
        switch ($request->type)
        {
            case 'date':
                session()->put(self::FILTER_DATE_KEY, $request->sort);
                break;
            case 'filter':
                session()->put(self::FILTER_OTHER_KEY, $request->sort);
                break;
            case 'tag':
                session()->put(self::FILTER_TAG_KEY, $request->sort);
                break;
        }
    }
}