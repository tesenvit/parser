<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller
{
//    public function __construct()
//    {
//        $this->middleware($this->guestMiddleware(), ['except' => 'getLogout']);
//    }
    public function getLogout()
    {
        Auth::logout();
        return redirect()->back();
    }

    public function saveRefererToSession(Request $request)
    {
        if($request->ajax() && $request->has('referer'))
        {
            $url = $request->referer;
        }
        else
        {
            $url = '/';
        }

        session()->put('refererURL', $url);
    }
}
