<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Model\Fornt\UserProvider;
use App\User;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;
use Laravel\Socialite\Facades\Socialite;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function handleProviderCallback($provider)
    {
        $user = Socialite::driver($provider)->user();
        $authUser = $this->findOrCreateUser($user, $provider);
        Auth::login($authUser, true);
        return redirect($this->getRedirectTo());
    }

    public function findOrCreateUser($user, $provider)
    {
        $authUser = UserProvider::where('provider_id', $user->id)->first();
        if ($authUser) {
            User::where('id', $authUser->user_id)->update([
                'avatar' => $user->avatar
            ]);
            return User::where('id', $authUser->user_id)->first();
        }
        $user_provader = User::create([
            'name' => $user->name,
            'email' => $user->email,
            'avatar' => $user->avatar
        ]);
        UserProvider::create([
            'provider' => $provider,
            'provider_id' => $user->id,
            'user_id' => $user_provader->id
        ]);
        return $user_provader;
    }

    private function getRedirectTo()
    {
        return session()->has('refererURL') ? session('refererURL') : '/';
    }
}
