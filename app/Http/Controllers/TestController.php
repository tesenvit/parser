<?php

namespace App\Http\Controllers;

use App\Traits\BustGetUrl;
use App\Traits\HtmlDomParsing;

class TestController extends Controller
{
    use HtmlDomParsing, BustGetUrl;

    protected $socialTags = ['article:tag', 'article:section'];
    protected $socialTagsSection = ['article:section'];
    protected $socialTagArt = ['og:image'];
    protected $publishedTime = ['article:published_time'];
    protected $datePublished = ['datePublished'];
    protected $charset = ['content-type'];
    private $alphabetRus = '"АаБбВвГгДдЕеЁёЖжЗзИиЙйКкЛлМмНнОоПпРрСсТтУуФфХхЦцЧчШшЩщЪъЫыЬьЭэЮюЯя"';
    protected $dom = null;

    private $requiredFileds = [
        'loc',
        'lastmod',
        'changefreq',
        'priority'
    ];

    const ANALYSIS_LINK_LIMIT = 700;
    const ANALYSIS_CONTENT_LIMIT = 700;
    const ANALYSIS_TAG_LIMIT = 600;
    const LIMIT_URLS_TO_TMP_TABLE = 200;
    const LIMIT_URLS_FROM_TMP_TABLE = 200;
    public $urls = [];


    /** DATA FOR LOGGER **/
    private $checkDateFromOG = 0;
    private $checkDateFromSchema = 0;
    private $checkDateFromDB = 0;

    protected $isSitemap = false;

    protected $analysisContentCount = 0;
    protected $allLinksCount = 0;
    protected $allLinksApprovedCount = 0;
    protected $allLinksNewsCount = 0;

    protected $updateOrCreateSec = 0;

    /*
     * auditor_count - счетчики
     * auditor_domain (вторую очередь)
     * auditor_error_url - HTTP коды ответов ссылок
     * auditor_url_anhors - она не нигде не используется
     * auditor_url_children - ссылки
     * jobs - системные данные
     * jeweler_content - контент
     * parser_logger_cycles - данные логера
     * parser_logger_cycles_processes - данные логера
     * tags - тэги
     * tags_section
     * tags_urls
     * manual_tags_organizer
     *
     * sitemaps - сайтмапы домена
     * sitemap_urls - урлы сайтмапа домена
     *
     * DB unni_tmp_url - ссылки для парсинга
     * ElasticSearch - jeweler_content
     *
     *

    $dd = AuditorDomains::where('type', '!=', 'admin')->get();
    foreach ($dd as $d)
        $d->delete();

     * */



    public function 🔧🔨⚒()
    {


    }



    private function showIteration($i, $cyclesCount)
    {
        ini_set('max_execution_time', 0);
        set_time_limit(0);

        echo '<script id="scri">document.getElementById("remove").remove();document.getElementById("scri").remove()</script>';
        echo '<div id="remove">';
        echo ++$i . ' из ' . $cyclesCount;
        echo '</div>';
        ob_flush();
    }
}
