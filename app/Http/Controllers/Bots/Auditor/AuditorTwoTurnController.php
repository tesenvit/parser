<?php

namespace App\Http\Controllers\Bots\Auditor;

use App\Model\Bots\Auditor\AuditorContent;
use App\Model\Bots\Auditor\AuditorDomains;
use Curl\MultiCurl;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AuditorTwoTurnController extends Controller
{
    public function index()
    {
        $domains = AuditorDomains::where('remote', 1)->where('count', 0)->limit(100)->get();
        $this->parse_сron($domains);
    }
    protected function parse_сron($allLinks)
    {
        // Запросы отправляються параллельно с функциями обратного вызова.
        $headers = array();
        $headers[] = 'Accept: text/html';
        $headers[] = 'Content-Type: text/html';
        $multi_curl = new MultiCurl();
        $multi_curl->setReferer(true);
        $multi_curl->setTimeout(60);
        $multi_curl->checkSsl();
        $multi_curl->setConnectTimeout(60);
        $multi_curl->setOpt(CURLOPT_FOLLOWLOCATION, true);
        $multi_curl->setEncoding('');
        $multi_curl->setVerifyHost(FALSE);
//        $multi_curl->setEncoding('gzip, deflate');
//        $multi_curl->setAutoReferrer(true); в новом пакете функция удалена
//        $multi_curl->setFollowLocation(true); в новом пакете функция удалена
        $multi_curl->setHeader('Content-Type', 'text/html');
        $multi_curl->setUserAgent('Mozilla/5.0 (Windows NT 6.1; rv:8.0) Gecko/20100101 Firefox/8.0');
        $multi_curl->success(function ($instance) {
            $this->saveContent($instance);
            unset($instance);
        });
        $multi_curl->error(function ($instance) {
            unset($instance);
        });

        $multi_curl->complete(function ($instance) {
        });
        foreach ($allLinks as $allLink) {
                $allLink->url = 'http://' . $allLink->domain;
                $allLink->url_id = 'home';
                $allLink->id_domain = $allLink->id;
                $multi_curl->addGet($allLink->url, $allLink->url_id, $allLink->urlsss, $allLink->id_domain);
        }
        // Блокирует до тех пор, пока все элементы в очереди не будут обработаны.
        $multi_curl->start();
    }
    protected function saveContent($instance)
    {
        $text = $instance->rawResponse;
        if ($instance->contentType == 'text/html; charset=windows-1251') {
            $text = @iconv('CP1251', 'UTF-8', $text);
        }
        $content = AuditorContent::create(
            [
                'url_id' => $instance->url_id,
                'content' => $text,
                'domain_id' => $instance->id_domain,
            ]
        );
        AuditorDomains::where('id', $instance->id_domain)->update([
            'count' => 1
        ]);
    }
}
