<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\StoreTagRequest;
use App\Model\Tags;
use Illuminate\Http\Request;

class TagsController extends Controller
{
    public function index()
    {
        $tags =  \App\Model\Admin\Tag::orderBy('count','desc')->paginate(50);

        return view('Admin.Tags.index', compact('tags'));
    }

    public function tagForm()
    {
        return view('Admin.Tags.Forms.addTag');
    }

    public function addTag(Request $request)
    {
        if (!empty($request->addTag))
        {
            $existingTags = [];
            $addedTags = [];

            foreach ($request->addTag as $addTag)
            {
                $addTag = mb_strtolower($addTag);

                $existTag = Tags::where('tag', $addTag)->first();
                if(empty($existTag))
                {
                    Tags::create(['tag' => $addTag, 'type' => 'hand']);
                    $addedTags[] = $addTag;
                }
                else
                {
                    $existingTags[] = $addTag;
                }
            }

            $status = '';
            if($existingTags)
                $status .= 'Данные теги уже существуют: ' . implode (', ', $existingTags) . '<br>';

            if($addedTags)
                $status .= 'Данные теги были добавлены: ' . implode (', ', $addedTags) . '<br>';

            return redirect('/test/tags/add_tags')->with('status', $status);
        }
        else
        {
            return view('Admin.Tags.addTags');
        }
    }

    public function delete($id){
        $deleteTag = Tags::find($id);
        $deleteTag->delete();
        return redirect('/test/tags')->with('status','Тег удален');
    }

    public function getTags(Request $request)
    {
        if($request->ajax())
        {
            $tagsCollection = Tags::where('tag', 'LIKE', '%'.$request->text.'%')
                                  ->orderByDesc('count')
                                  ->limit(10)
                                  ->get();

            $tags = $this->transformDataForTokenize2($tagsCollection);

            return response()->json($tags);
        }
    }

    private function transformDataForTokenize2($tags)
    {
        $result = [];

        foreach ($tags as $tag)
        {
            $result[] = [
                'text' => $tag->tag,
                'value' => $tag->id
            ];
        }

        return $result;
    }

    public function showSwitchActive(Request $request)
    {
        if($request->tagId)
        {
            $tag = Tags::where('id', $request->tagId)->first();
            $tag->show = $tag->show ? 0 : 1;
            $tag->save();
        }
    }

    public function search(Request $request)
    {
        if($request->ajax())
        {
            $search = $request->search;

            $tags = Tags::where('tag', 'LIKE', '%' . $search . '%')
                        ->orderBy('count','desc')
                        ->limit(50)
                        ->get();

            return view('Admin.Tags.content', compact('tags'))->render();
        }
    }
}
