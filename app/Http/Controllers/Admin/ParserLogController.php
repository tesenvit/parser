<?php

namespace App\Http\Controllers\Admin;

use App\Components\ParserLogger\Models\ParserLoggerCycle;
use App\Components\ParserLogger\Models\ParserProcessManager;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ParserLogController extends Controller
{
    public function index()
    {
        $processes = ParserProcessManager::all();

        return view('Admin.Statistics.ParserLog.index', compact('processes'));
    }

    public function showProcessLogs($id)
    {
        $processManager = ParserProcessManager::find($id);

        $cycles = ParserLoggerCycle::where('processes_id', $id)->orderBy('id', 'desc')->limit(50)->get();

        return view('Admin.Statistics.ParserLog.show_process', compact('cycles', 'processManager'));
    }

    public function switchActive(Request $request)
    {
        $process = ParserProcessManager::find($request->processId);

        if($process)
        {
            $process->is_active_logger = ($process->is_active_logger) ? 0 : 1;
            $process->save();
        }

        $processes = ParserProcessManager::all();
        return view('Admin.Statistics.ParserLog.processes_list', compact('processes'));
    }
}
