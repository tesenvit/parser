<?php

namespace App\Http\Controllers\MinerBot;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
//use App\Traits\BustGetUrl;
use Symfony\Component\DomCrawler\Crawler;
class MinerController extends Controller
{
//    use BustGetUrl;
    public function index()
    {
        $start = microtime(true);
        $cronStart = DB::table('miner_cron')->orderby('id', 'desc')->first();
            $cronLevel = $cronStart->level;
            $cronStart = $cronStart->start;

        $cronStart++;
        if ($cronLevel == 0){
            DB::table('miner_cron')->insert([
            'level' => '1',
            'start' => $cronStart,
            'updated_at' => date('Y-m-d G:i:s'),
            'created_at' => date('Y-m-d G:i:s')
        ]);
            $cronStarts= DB::table('miner_content')->pluck('id_url');
//            dd($cronStart);
            $allLinks = DB::table('auditor_url_children')
                ->orderBy('id')
                ->where([
                    ['http_code', '<>', '404'],
                    ['http_code', '<>', '302'],
                ])
                ->whereNotIn('id', $cronStarts)
                ->limit(10)->get();
                    foreach ($allLinks as $link) {
                        $httpCode = $this->get_http_response_code($link->url);

                        if ($this->get_http_response_code($link->url) == "200") {
                            $html = file_get_contents($link->url);

                        $crawler = new Crawler(null, $link->url);
                        $crawler->addHtmlContent($html, 'UTF-8');

                        $title = $crawler->filter('title')->text();
                        $meta = $crawler->filterXpath('//meta[@name="description"]')->each(function ($node) {
                            return [
                                'content' => $node->attr('content'),
                            ];
                        });

                        $metaDescription = array();
                        foreach ($meta as $subArr) {
                            foreach ($subArr as $key => $val) {
                                if (isset($metaDescription[$key]) && $metaDescription[$key] > $val) continue;
                                $metaDescription[$key] = $val;
                            }
                        }
                        $metaDescription = implode('', $metaDescription);

                        if (!DB::table('miner_content')->where('id_url', $link->id)->first()) {
                            $cronStart = DB::table('miner_cron')->orderby('id', 'desc')->first();
                            $cronStart = $cronStart->start;
                            DB::table('miner_content')->insert([
                                'id_url' => $link->id,
                                'title' => $title,
                                'description' => $metaDescription,
                                'cron_start' => $cronStart,
                                'updated_at' => date('Y-m-d G:i:s'),
                                'created_at' => date('Y-m-d G:i:s')
                            ]);
                        }
                    }else{
                            DB::table('auditor_url_children')
                                ->where('id', $link->id)
                                ->update(['http_code' => $httpCode]);
                        }
                        sleep(3);
                }

            DB::table('miner_cron')->insert([
                'level' => '0',
                'start' => $cronStart,
                'updated_at' => date('Y-m-d G:i:s'),
                'created_at' => date('Y-m-d G:i:s')
            ]);
        }

        $sum = 0;
        for ($i = 0; $i < 100000; $i++) $sum += $i;
        echo "Время выполнения скрипта: ".(microtime(true) - $start);
    }
    public function get_http_response_code($url) {
        $headers = get_headers($url);
        return substr($headers[0], 9, 3);
    }
}
