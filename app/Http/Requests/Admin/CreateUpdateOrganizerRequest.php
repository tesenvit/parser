<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class CreateUpdateOrganizerRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|string',
            'website' => 'required|string',
            'fb_group_id' => 'required|string|unique:organizers,fb_group_id,' . $this->getOrganizerId(),
            'country' => 'string',
            'city' => 'string',
        ];
    }

    private function getOrganizerId()
    {
        return $this->organizer ? $this->organizer->id : '';
    }
}
