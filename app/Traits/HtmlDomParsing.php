<?php

namespace App\Traits;

use DOMDocument;
use DOMXPath;

trait HtmlDomParsing
{
    protected $socialTags = ['article:tag', 'article:section'];
    protected $socialTagsSection = ['article:section'];
    protected $socialTagArt = ['og:image'];
    protected $publishedTime = ['article:published_time'];
    protected $datePublished = ['datePublished'];
    protected $charset = ['content-type'];

    protected $dom = null;

    /**
     * Создаем экземпляр объектной модели HTML
     *
     * @param $content
     * @return void
     */
    protected function domLoadHtml($content)
    {
        $this->dom = new DOMDocument('1.0', 'UTF-8');
        $this->dom->preserveWhiteSpace = false;

        @$this->dom->loadHTML('<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />' . $content);
    }

    /**
     * Забираем <title>
     *
     * @return null|string
     */
    protected function title() :?string
    {
        if ($this->dom)
        {
            $list = $this->dom->getElementsByTagName("title");

            if ($list->length > 0)
                return trim($list->item(0)->textContent);
        }

        return null;
    }

    /**
     * Забирем <meta> c описанием страницы
     *
     * @return \DOMNodeList
     */
    protected function description()
    {
        $descriptions = $this->dom->getElementsByTagName('meta');
        foreach ($descriptions as $node)
        {
            if ($node->getAttribute('name') == 'description')
                return $node->getAttribute('content');

             elseif ($node->getAttribute('name') == 'Description')
                return $node->getAttribute('content');
        }
    }

    /**
     * Собираем ключевые слова, если есть
     *
     * @param $content
     *
     * @return array|null
     */
    protected function keywords($content) :?array
    {
        $this->domLoadHtml($content);

        if($this->dom)
        {
            $keyword = $this->dom->getElementsByTagName('meta');

            $tags = [];

            foreach ($keyword as $key => $node)
            {
                $keywords = $this->getKeywords($node);
                if($keywords)
                    return $keywords;

                foreach ($this->socialTags as $socialTag)
                    if ($node->getAttribute('property') == $socialTag)
                        $tags[] = $node->getAttribute('content');
            }

            if (!empty($tags))
                return $tags;
        }

        return null;
    }

    /**
     * Забираем строку с ключевыми словами из keywords или Classification
     *
     * @param $node
     *
     * @return array|null
     */
    private function getKeywords($node)
    {
        $keywords = $this->getDataByAttribute($node,'keywords');

        if(!$keywords)
            $keywords = $this->getDataByAttribute($node,'Classification');

        if ($keywords && !is_array($keywords))
            return explode(',', $keywords);

        return $keywords;
    }

    /**
     * Ищем данные по атрибуту
     *
     * @param $node
     * @param $attribute
     *
     * @return array|null
     */
    private function getDataByAttribute($node, $attribute)
    {
        $keywords = null;

        if ($node->getAttribute('name') == $attribute)
        {
            $keywords = $node->getAttribute('content');

            if (strlen($keywords) > 1)
                $keywords = explode(',', mb_strtolower($keywords));
        }

        return $keywords;
    }

    /**
     * Выбырием из OpenGraph название статьи, если есть.
     *
     * @return array
     */
    protected function section() :array
    {
        $keywords = $this->dom->getElementsByTagName('meta');

        $tags = [];
        foreach ($keywords as $node)
        {
            foreach ($this->socialTagsSection as $socialTag)
            {
                if ($node->getAttribute('property') == $socialTag)
                    $tags[] = $node->getAttribute('content');
            }
        }

        return $tags;
    }

    /**
     * Пробуем забрать дату из OpenGraph
     *
     * @return array|null
     */
    protected function publishedDate() :?array
    {
        $keywords = [];
        $keyword = $this->dom->getElementsByTagName('meta');

        foreach ($keyword as $node)
        {
            foreach ($this->publishedTime as $socialTag)
            {
                if ($node->getAttribute('property') == $socialTag)
                    $keywords[] = $node->getAttribute('content');
            }

            if ($node->getAttribute('property') == $socialTag)
                return $keywords;
        }

        return $keywords;
    }

    /**
     * Пробуем забрать дату из Schema
     *
     * @return array|null
     */
    protected function publishedDateTwo() :?array
    {
        $keywords = [];
        $keyword = $this->dom->getElementsByTagName('meta');

        foreach ($keyword as $node)
        {
            foreach ($this->datePublished as $socialTag)
            {
                if ($node->getAttribute('itemprop') == $socialTag)
                    $keywords[] = $node->getAttribute('content');
            }

            if ($node->getAttribute('itemprop') == $socialTag)
                return $keywords;
        }

        return $keywords;
    }

    /**
     * Забираем картинку, еслие есть
     *
     * @return mixed
     */
    protected function getImage()
    {
        $keyword = $this->dom->getElementsByTagName('meta');

        foreach ($keyword as $node)
        {
            foreach ($this->socialTagArt as $socialTag)
            {
                if ($node->getAttribute('property') == $socialTag)
                    $keywords[] = $node->getAttribute('content');
            }

            if ($node->getAttribute('property') == $socialTag)
                return $keywords[0];

        }
    }

    /**
     * Возвращает массив всех ссылок с переданного HTML
     *
     * @param $content
     * @return array|null
     */
    function getLinks($content) :?array
    {
        $dom = new DOMDocument;
        libxml_use_internal_errors(true);
        @$dom->loadHTML('<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />' . $content);
        $xpath = new DOMXPath($dom);

        $hrefs = $xpath->evaluate("/html/body//a");
        for ($i = 0; $i < $hrefs->length; $i++)
        {
            $href = $hrefs->item($i);
            $url = $href->getAttribute('href');

            // Анкор ссылки
            if ($url)
            {
                $urlss[] = $this->deleteСharactersNT($href->nodeValue);

                // Сама ссылка
                $urls[] = $url;
            }
        }

        if (isset($urls))
        {
            $urls = array_combine($urls, $urlss);
            if (isset($urls)) return $urls;
        }

        return null;
    }

    /**
     * Удаляем спецсимолы
     *
     * @param $str
     *
     * @return string
     */
    private function deleteSpecialСharacters($str) :string
    {
        $str = mb_strtolower($str);
        $str = trim($str, ' .,"!@#$%^&*()_+-=/');
        return str_replace('`', '\'', $str);
    }

    /**
     * Удялем пробелы и перводы строк
     *
     * @param $text
     *
     * @return string
     */
    public function deleteСharactersNT($text) :string
    {
        return trim($text, " \t\n\r\0\x0B\xC2\xA0");
    }
}