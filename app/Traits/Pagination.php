<?php
/**
 * Created by PhpStorm.
 * User: Smm3
 * Date: 13.08.2018
 * Time: 10:27
 */

namespace App\Traits;

trait Pagination
{

    function generateSmartPaginationPageNumbers($total, $perPage = null, $current = null, $adjacentNum = null)
    {
        $result = [];

        if (isset($total, $perPage) === true) {
            $result = range(1, ceil($total / $perPage));
            if (isset($current, $adjacentNum) === true) {
                if (($adjacentNum = floor($adjacentNum / 2) * 2 + 1) >= 1) {
                    $result = array_slice($result, max(0, min(count($result) - $adjacentNum, intval($current) - ceil($adjacentNum / 2))), $adjacentNum);
                }
            }
        }

        if (!in_array(1, $result)) {
            if ($result[0] > 3) {
                array_unshift($result, '');
            }

            if ($result[0] == 3) {
                array_unshift($result, 2);
            }

            array_unshift($result, 1);
        }

        $totalPagesNum = ceil($total/$perPage);
        $lastMiddlePageNum = $result[count($result)-1];
        if (!in_array($totalPagesNum, $result)) {
            if ($totalPagesNum - $lastMiddlePageNum > 2) {
                $result[] = '';
            }

            if ($totalPagesNum - $lastMiddlePageNum == 2) {
                $result[] = $lastMiddlePageNum + 1;
            }

            $result[] = $totalPagesNum;
        }

        return $result;
    }
}