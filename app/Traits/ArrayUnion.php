<?php
/**
 * Created by PhpStorm.
 * User: www
 * Date: 03.01.19
 * Time: 15:33
 */

namespace App\Traits;


trait ArrayUnion
{
    public function arrayUnion($arrayOne, $arrayTwo)
    {
        $groupedArrays = array($arrayOne, $arrayTwo);
        $maxArrayItems = max(array_map(function($array) {
            return count($array);
        }, $groupedArrays));
        $new = array();
        for ($i=0; $i < $maxArrayItems; $i++) {
            for($j=0; $j < count($groupedArrays); $j++) {
                if(isset($groupedArrays[$j][$i])) {
                    $new[] = $groupedArrays[$j][$i];
                }
            }
        }
        return $new;
    }
}