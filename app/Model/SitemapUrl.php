<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class SitemapUrl extends Model
{
    protected $table = 'sitemap_urls';

    protected $fillable = ['loc', 'lastmod', 'priority', 'changefreq', 'sitemap_id'];
}
