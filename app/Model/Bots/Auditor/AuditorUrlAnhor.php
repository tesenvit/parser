<?php

namespace App\Model\Bots\Auditor;

use Illuminate\Database\Eloquent\Model;

class AuditorUrlAnhor extends Model
{
    protected $fillable = ['url_id', 'anchor', 'domain_id'];
}
