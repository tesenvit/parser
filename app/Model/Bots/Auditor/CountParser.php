<?php

namespace App\Model\Bots\Auditor;

use Illuminate\Database\Eloquent\Model;

class CountParser extends Model
{
    protected $table = 'auditor_count';
    public $timestamps = false;
    protected $fillable = ['id_domain', 'count_content', 'count_link'];
}
