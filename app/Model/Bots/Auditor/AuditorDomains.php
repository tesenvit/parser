<?php

namespace App\Model\Bots\Auditor;

use App\Model\Bots\Miner\MinerContent;
use App\Model\Sitemap;
use App\Model\SitemapUrl;
use Illuminate\Database\Eloquent\Model;

class AuditorDomains extends Model
{
    protected $table = 'auditor_domain';

    protected $fillable = ['domain', 'ping', 'priority', 'parser_content', 'remote', 'type', 'count', 'httpPort'];

    public function auditorUrls()
    {
        return $this->hasMany(AuditorUrls::class, 'id_domain', 'id')->select('id');
    }

    public function auditorContents()
    {
//        return $this->belongsToMany(MinerContent::class, 'auditor_url_children', 'id_domain', 'id', 'id_url', 'id');
        return $this->hasManyThrough(MinerContent::class, AuditorUrls::class, 'id_domain', 'id_url')->select('id_url');
    }

    public function sitemaps()
    {
        return $this->hasMany(Sitemap::class, 'domain_id','id');
    }

    public function sitemapsUrls()
    {
        return $this->hasManyThrough(SitemapUrl::class, Sitemap::class,'domain_id','sitemap_id','id');
    }

}
