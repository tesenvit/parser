<?php

namespace App\Model\Bots\Jeweler;

use Illuminate\Database\Eloquent\Model;

class Tag extends Model
{
    protected $fillable = ['tag', 'type','count'];
    public function urls()
    {
        return $this->belongsToMany('App\Model\Bots\Jeweler\Tag', 'tags_urls', 'tag_id', 'url_id');
    }
}
