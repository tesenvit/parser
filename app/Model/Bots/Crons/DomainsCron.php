<?php

namespace App\Model\Bots\Crons;

use Illuminate\Database\Eloquent\Model;

class DomainsCron extends Model
{
    protected $table = 'auditor_cron';
}
