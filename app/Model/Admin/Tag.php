<?php

namespace App\Model\Admin;

use Illuminate\Database\Eloquent\Model;

class Tag extends Model
{
    protected $fillable = ['tag', 'type', 'url_id'];
    public function pageTags()
    {
        return $this->belongsToMany(Tag::class, 'tags_urls');
    }

//    public function pageTags($tag_id)
//    {
//        return DB::table('tags_urls')->where('tag_id', $tag_id);
//    }
}
