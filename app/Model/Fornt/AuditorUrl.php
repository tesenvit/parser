<?php

namespace App\Model\Fornt;

use App\Model\Bots\Jeweler\JewelerContent;
use App\Model\Tags;
use Illuminate\Database\Eloquent\Model;

class AuditorUrl extends Model
{
    protected $table = 'auditor_url_children';

    public function urlContent()
    {
        return $this->hasOne(Feed::class, 'id_url', 'id');
    }

    public function content()
    {
        return $this->hasOne(JewelerContent::class, 'id_url','id');
    }

    public function tags()
    {
        return $this->belongsToMany(Tags::class,'tags_urls','url_id','tag_id');
    }
}
