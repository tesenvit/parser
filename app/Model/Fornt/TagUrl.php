<?php

namespace App\Model\Fornt;

use Illuminate\Database\Eloquent\Model;

class TagUrl extends Model
{
   protected $table = 'tags_urls';

   protected $with = ['tag'];

   protected static function boot()
   {
       parent::boot();

       static::addGlobalScope('hasName',function ($q){
           $q->whereHas('tag');
       });
   }

    public function tag()
   {
       return $this->belongsTo(TagSection::class, 'tag_id','id');
   }

   public function getNameAttribute()
   {
        return $this->tag->name;
   }
}
