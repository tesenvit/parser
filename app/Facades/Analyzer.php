<?php

namespace App\Facades;

use App\Components\Analyzer\RobotTxt;
use App\Components\Analyzer\Sitemap;
use Illuminate\Support\Facades\Facade;

/**
 * Class Analyzer
 * @package App\Facades
 *
 * @method static RobotTxt robotTxt();
 * @method static Sitemap sitemap();
 */
class Analyzer extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'analyzerFactory';
    }
}