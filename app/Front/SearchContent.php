<?php

namespace App\Front;

use App\Model\Bots\Auditor\AuditorUrls;
use App\Model\Tags;
use App\TutorialIndexConfigurator;
use Illuminate\Database\Eloquent\Model;

use Illuminate\Support\Facades\DB;
use Laravel\Scout\Searchable;

class SearchContent extends Model
{
    use Searchable;

    public $fillable = [
        'title',
        'description'
    ];

    protected $table = 'miner_content';

    protected $indexConfigurator = TutorialIndexConfigurator::class;
    protected $mapping = [
        'properties' => [
            'id' => [
                'type' => 'integer',
                'index' => 'my_index'
            ],
            'name' => [
                'type' => 'title',
                'analyzer' => 'russian'
            ]
        ]
    ];
    public function urls()
    {
        return $this->hasMany(AuditorUrls::class);
    }


//    public function searchableAs()
//    {
//        return 'title_index';
//    }

    public function auditorUrls()
    {
        $url =  $this->hasOne(AuditorUrls::class, 'id', 'id_url')->select('url', 'domain')->join('auditor_domain', 'auditor_url_children.id_domain', '=', 'auditor_domain.id');
        return $url;
    }
    public function tagsUrls()
    {
        return $this->hasMany(Tags::class, 'url_id', 'id_url')->limit(5);
    }
}
