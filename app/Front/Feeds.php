<?php

namespace App\Front;

use App\Model\Bots\Auditor\AuditorUrls;
use Illuminate\Database\Eloquent\Model;

class Feeds extends Model
{
    protected $table = 'jeweler_content';
    public function linksContent()
    {
        return $this->hasOne(AuditorUrls::class, 'id', 'id_url');
    }
}
