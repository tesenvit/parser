<?php

namespace App\Exceptions;

use Exception;

class UnknownFormat extends Exception
{
    protected $code = 500;

    public function __construct(string $message = "", string $format)
    {
        $this->message = $message . ' Format: ' . $format;
    }
}
