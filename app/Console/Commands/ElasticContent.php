<?php

namespace App\Console\Commands;

use App\Components\ParserLogger\ParserLoggerFacade;
use App\Http\Controllers\Bots\ElasticIndex\ElasticMetaController;

class ElasticContent extends ParserCommand
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'elasticContent';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        parent::handle();

        /** START LOGGER **/
        $id = ParserLoggerFacade::startProcess('Записываем индексы');
        /** ************ **/

        (new ElasticMetaController())->elasticIndexisContent();

        /** STOP LOGGER **/
        ParserLoggerFacade::stopProcess($id);
        /** ************ **/
    }
}
