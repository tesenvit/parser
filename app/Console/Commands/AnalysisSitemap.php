<?php

namespace App\Console\Commands;

use App\Http\Controllers\Bots\SitemapParser\SitemapParser;

class AnalysisSitemap extends ParserCommand
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'analysis_sitemap';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Alasysis Sitemap';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        parent::handle();

        (new SitemapParser())->analysis();
    }
}
