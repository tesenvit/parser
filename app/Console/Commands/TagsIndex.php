<?php

namespace App\Console\Commands;

use App\Http\Controllers\Bots\JewelerBot\TagsController;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class TagsIndex extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'start_tags_index';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command start tags MODEL index elastic search and select tags is html';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
            $tmpTable = new TagsController();
            $tmpTable->analysisTags();
    }
}
