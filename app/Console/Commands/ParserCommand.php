<?php
/**
 * Created by PhpStorm.
 * User: Des-2
 * Date: 06.06.2019
 * Time: 17:02
 */

namespace App\Console\Commands;


use App\Components\ParserLogger\ParserLogger;
use Illuminate\Console\Command;

abstract class ParserCommand extends Command
{
    public function handle()
    {
        app()->singleton('parser_logger', function () {
            return new ParserLogger($this->signature);
        });
    }
}