<?php

namespace App\Console\Commands;

use App\Components\ParserLogger\ParserLoggerFacade;
use App\Http\Controllers\Bots\Auditor\AuditorUrlsController;

class tmpTable extends ParserCommand
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'tmp_table_parser';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command created tmp table in new base(unni_tmp_url)';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        parent::handle();

        /** START LOGGER **/
        $id = ParserLoggerFacade::startProcess('Создание временных таблиц');
        /** ************ **/

        (new AuditorUrlsController())->tableTmp();

        /** STOP LOGGER **/
        ParserLoggerFacade::stopProcess($id);
        /** ************ **/
    }
}
