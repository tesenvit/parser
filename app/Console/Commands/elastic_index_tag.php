<?php

namespace App\Console\Commands;

use App\Http\Controllers\Bots\JewelerBot\TagsController;
use Illuminate\Console\Command;

class elastic_index_tag extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'elastic_index_tag';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $urlParser = new TagsController();
        $urlParser->elasticIndexis();
    }
}
