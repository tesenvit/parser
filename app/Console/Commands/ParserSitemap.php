<?php

namespace App\Console\Commands;


use App\Http\Controllers\Bots\SitemapParser\SitemapParser;

class ParserSitemap extends ParserCommand
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'parser_sitemap';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'parser sitemap';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return  void
     */
    public function handle() :void
    {
        parent::handle();

        (new SitemapParser())->parse();
    }
}