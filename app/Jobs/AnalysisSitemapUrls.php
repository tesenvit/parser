<?php

namespace App\Jobs;

use App\Http\Controllers\Bots\JewelerBot\JewelerController;
use App\Model\Bots\Auditor\AuditorDomains;
use App\Model\Bots\Auditor\AuditorUrls;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class AnalysisSitemapUrls implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $urls;

    private $domain;

    public $retryAfter = 3600; // 2 hours

    public $tries = 2;

    public $timeout = 600;

    /**
     * Create a new job instance.
     *
     * @param object collection $urls
     * @param AuditorDomains $domain
     */
    public function __construct($urls, AuditorDomains $domain)
    {
        $this->urls = $urls;
        $this->domain = $domain;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $jeweler = new JewelerController();

        foreach ($this->urls as $url)
        {
            $clearUrl = $jeweler->relateDomainUrl($this->domain->domain, $url->loc, $this->domain->httpPort, $this->domain->level);

            if($clearUrl)
                $jeweler->storeLink($clearUrl, $this->domain);

            $url->parsing = 1;
            $url->save();
        }

        AuditorUrls::insert($jeweler->urls);
    }
}
